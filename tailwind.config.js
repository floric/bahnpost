/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{ts,tsx}"],
  theme: {
    extend: {
      minWidth: {
        xl: "36rem",
      },
      maxWidth: {
        "8xl": "96rem",
      },
      minHeight: {
        md: "18rem",
      },
    },
  },
  plugins: [],
};
