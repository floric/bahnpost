/* @refresh reload */
import "./index.css";
import { render } from "solid-js/web";
import { loadWorkspace } from "./ipc/bindings.generated";
import { setLoadedWorkspace } from "./stores/workspace";

const startAsync = async () => {
  const [App, workspace] = await Promise.all([
    import("./App"),
    loadWorkspace("dev-ws.json"),
  ]);

  setLoadedWorkspace(workspace);

  render(() => <App.default />, document.getElementById("root") as HTMLElement);
};

startAsync();
