import { Alert, Button } from "@kobalte/core";
import { Route, Router } from "@solidjs/router";
import { ErrorBoundary, lazy, Match, Show, Switch } from "solid-js";
import { dismissError, handleError, catchedError } from "./utils/errorhandling";

export const routes: Array<{ path: string; header: string }> = [
  { header: "Explorer", path: "/" },
  { header: "Environments", path: "/environments" },
  { header: "Collections", path: "/collections" },
  { header: "Tests", path: "/tests" },
];

const Explorer = lazy(() => import("./views/Explorer"));
const Environments = lazy(() => import("./views/Environments"));
const Collections = lazy(() => import("./views/Collections"));
const Tests = lazy(() => import("./views/Tests"));

/**
 * Catches all previously uncatched runtime errors.
 *
 * @param err error
 */
window.onerror = (err) => handleError(err, { isUnexpected: true });

const App = () => {
  return (
    /** prevent text selection **/
    <div class="text-gray-900 select-none">
      <Show when={catchedError().error} keyed>
        {(err) => (
          <Alert.Root class="bg-red-600 border border-red-700 text-white p-4 m-4 fixed shadow flex gap-4 justify-between items-baseline rounded-lg left-0 right-0">
            <div>
              <Switch>
                <Match when={err.isUnexpected}>
                  <span class="font-bold">Unexpected Error:</span> {err.message}{" "}
                  <Show when={err.stack} keyed>
                    {(s) => <>({s})</>}
                  </Show>
                </Match>
                <Match when={!err.isUnexpected}>
                  <span class="font-bold">{err.message}</span>
                </Match>
              </Switch>
            </div>
            <Button.Root
              class="font-bold border border-white rounded px-2 py-1 hover:bg-red-500"
              onClick={dismissError}
            >
              Dismiss
            </Button.Root>
          </Alert.Root>
        )}
      </Show>

      {/** Add routing later here */}
      <ErrorBoundary
        fallback={(err) => {
          handleError(err);
          return null;
        }}
      >
        <Router>
          <Route path="/" component={Explorer} />
          <Route path="/environments" component={Environments} />
          <Route path="/collections" component={Collections} />
          <Route path="/tests" component={Tests} />
        </Router>
      </ErrorBoundary>
    </div>
  );
};

export default App;
