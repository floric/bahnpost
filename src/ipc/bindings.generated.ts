/* eslint-disable */
// This file was generated by [tauri-specta](https://github.com/oscartbeaumont/tauri-specta). Do not edit this file manually.

declare global {
  interface Window {
    __TAURI_INVOKE__<T>(
      cmd: string,
      args?: Record<string, unknown>
    ): Promise<T>;
  }
}

// Function avoids 'window not defined' in SSR
const invoke = () => window.__TAURI_INVOKE__;

/**
 * Updates a workspace and later persists it to the workspace folder.
 */
export function updateWorkspace(workspace: Workspace) {
  return invoke()<null>("update_workspace", { workspace });
}

/**
 * Loads a mocked workspace (which is a Git compatible folder with environments and request collections).
 * In the future, this method should be able to load a specific workspace based on a name or path.
 */
export function loadWorkspace(fileName: string) {
  return invoke()<Workspace>("load_workspace", { fileName });
}

export function parseValue(value: string, activeEnv: string | null) {
  return invoke()<ParseValueResult>("parse_value", { value, activeEnv });
}

export function sendRequest(data: RequestData) {
  return invoke()<SendRequestResult>("send_request", { data });
}

export function resolveRequest(data: RequestData) {
  return invoke()<ResolvedRequestData>("resolve_request", { data });
}

/**
 * Returns true if the public key has a valid format.
 */
export function isValidPublicKey(key: string) {
  return invoke()<boolean>("is_valid_public_key", { key });
}

export type HeaderEntry = { name: string; value: string };
export type ValueComponent =
  | {
      type: "variable";
      identifier: string;
      resolvedValue: string | null;
      env: string | null;
    }
  | ({ type: "computed" } & ComputedValue)
  | { type: "literal" };
export type SendRequestResult = {
  response: SendRequestResponse;
  requestData: RequestData;
  resolvedData: ResolvedRequestData;
  dateTime: string;
};
export type Variable = { key: string; value: string; isSecret: boolean };
export type Environment = {
  name: string;
  description: string | null;
  variables: Variable[];
};
export type RequestData = {
  uri: string;
  method: string;
  headers: HeaderEntry[];
  body: string | null;
  activeEnvName: string | null;
};
export type Workspace = {
  name: string;
  environments: Environment[];
  description: string | null;
  fileName: string;
  recipients: string[];
};
export type ValuePart = {
  value: string;
  start: number;
  end: number;
  component: ValueComponent;
};
export type ResolvedRequestData = { uri: string; headers: HeaderEntry[] };
export type SendRequestResponse =
  | { type: "error"; message: string; details: string | null }
  | {
      type: "success";
      status: number;
      statusText: string;
      headers: HeaderEntry[];
      body: string;
      bodySize: number;
      durationMs: number;
    };
export type ParseValueResult = { parts: ValuePart[] };
export type ComputedValue =
  | { mode: "uuidV4"; generated: string }
  | { mode: "randomAlphanumeric"; generated: string };
