import { format } from "date-fns";

export const formatFileSize = (size: number) => {
  const i = size == 0 ? 0 : Math.floor(Math.log(size) / Math.log(1024));
  return (
    Number(size / Math.pow(1024, i)).toFixed(2) +
    " " +
    ["B", "KB", "MB", "GB", "TB"][i]
  );
};

export const formatDuration = (durationInMs: number) => {
  return `${durationInMs.toFixed(0)} ms`;
};

export const formatDatetime = (datetime: string) => {
  return format(new Date(datetime), "HH:mm:ss dd/MM/yy");
};
