import { describe, expect, test } from "vitest";
import { formatDatetime, formatDuration, formatFileSize } from "./formatting";

describe("formatting", () => {
  describe("formatFileSize", () => {
    test("should format megabytes", () => {
      // when
      const formatted = formatFileSize(26_250_000);

      // then
      expect(formatted).toBe("25.03 MB");
    });

    test("should format kilobytes", () => {
      // when
      const formatted = formatFileSize(257_100);

      // then
      expect(formatted).toBe("251.07 KB");
    });

    test("should format bytes", () => {
      // when
      const formatted = formatFileSize(123);

      // then
      expect(formatted).toBe("123.00 B");
    });
  });

  describe("formatDuration", () => {
    test("should format milliseconds", () => {
      // when
      const formatted = formatDuration(123);

      // then
      expect(formatted).toBe("123 ms");
    });

    test("should format seconds", () => {
      // when
      const formatted = formatDuration(257_100);

      // then
      expect(formatted).toBe("257100 ms");
    });
  });

  describe("formatDatetime", () => {
    test("should format date", () => {
      // when
      const formatted = formatDatetime(
        new Date(2020, 4, 2, 1, 23, 54).toISOString()
      );

      // then
      expect(formatted).toBe("01:23:54 02/05/20");
    });
  });
});
