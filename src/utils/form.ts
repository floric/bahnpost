export type ValidationError<T> = {
  message: string;
  field: keyof T;
} | null;

export type FormValue<C> = {
  value: C;
  error: string | null;
};

export type FormData<T> = {
  [P in keyof T]: FormValue<T[P]>;
};

export type FormRules<T> = Array<{
  fn: (values: FormData<T>) => ValidationError<T>;
}>;
