import { createStore } from "solid-js/store";

const [unhandledError, setUnhandledError] = createStore<{
  error: {
    message: string;
    stack?: string;
    isUnexpected: boolean;
  } | null;
}>({ error: null });

export const handleError = (
  error: unknown,
  options?: { isUnexpected?: boolean }
) => {
  const isUnexpected = options?.isUnexpected || true;
  if (error instanceof Error) {
    setUnhandledError({
      error: {
        message: error.message,
        isUnexpected,
        stack: isUnexpected ? error.stack : undefined,
      },
    });
  } else if (typeof error === "string") {
    setUnhandledError({ error: { message: error as string, isUnexpected } });
  }

  console.error("error occured", error);
};

export const dismissError = () => {
  setUnhandledError({ error: null });
};

export const catchedError = () => unhandledError;
