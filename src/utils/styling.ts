export const inputBaseStyle =
  "text-sm rounded-lg p-2 appearance-none inline-flex gap-2 items-center justify-center";

// colors
export const inputGray =
  "bg-white border border-gray-300 hover:border-gray-400 hover:text-black";
export const inputPrimary =
  "bg-indigo-600 text-gray-50 border border-indigo-400 hover:bg-indigo-700 disabled:border-gray-500 disabled:bg-gray-700 hover:border-indigo-500 font-bold";
export const inputError = "outline outline-red-400 hover:border-red-500";

// buttons
export const buttonDanger =
  "bg-red-600 text-gray-50 border border-red-400 hover:bg-red-700 hover:border-red-500 font-bold";
