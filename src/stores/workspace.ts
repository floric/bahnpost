import { createEffect, createMemo } from "solid-js";
import { createStore, produce } from "solid-js/store";
import { type Workspace, updateWorkspace } from "../ipc/bindings.generated";

export type Variable = {
  key: string;
  value: string;
  isSecret: boolean;
};

export type Environment = {
  description: string | null;
  variables: Array<Variable>;
};

export type WorkspaceStore = {
  name: string | null;
  description: string | null;
  fileName: string | null;
  environments: Record<string, Environment>;
  recipients: Array<string>;
};

const [store, setStore] = createStore<WorkspaceStore>({
  name: null,
  fileName: null,
  description: null,
  environments: {},
  recipients: [],
});

createEffect(() => {
  console.log("WorkspaceStore changed", store);
  // prevent saving if workspace is not loaded yet
  if (!store.name || !store.fileName) {
    return;
  }
  const workspace: Workspace = {
    name: store.name,
    environments: Object.entries(store.environments).map(([name, env]) => ({
      name,
      variables: env.variables,
      description: env.description,
    })),
    fileName: store.fileName,
    description: store.description,
    recipients: store.recipients,
  };
  updateWorkspace(workspace);
});

export const setLoadedWorkspace = (workspace: Workspace) => {
  setStore({
    name: workspace.name,
    fileName: workspace.fileName,
    description: workspace.description,
    recipients: workspace.recipients,
    environments: workspace.environments
      .map(({ name, description, variables }) => ({
        [name]: {
          description,
          variables,
        },
      }))
      .reduce((a, b) => ({ ...a, ...b }), {}),
  });
};

export const loadedWorkspace = () => store;

export const addEnvironment = (name: string, description?: string) =>
  setStore(
    produce(
      (state) =>
        (state.environments[name] = {
          description: description || "",
          variables: [],
        })
    )
  );

export const deleteEnvironment = (name: string) =>
  setStore(
    produce((state) => {
      delete state.environments[name];
    })
  );

export const updateEnvironment = (
  name: string,
  oldName: string,
  description?: string
) =>
  setStore(
    produce((state) => {
      const existing = state.environments[name];
      state.environments[name] = {
        description: description || "",
        variables: existing?.variables || [],
      };
      if (oldName !== name && state.environments[oldName]) {
        delete state.environments[oldName];
      }
    })
  );

export const environments = createMemo(() => store);

export const environmentNames = createMemo(() =>
  Object.keys(store.environments)
);

export const updateVariable = (
  index: number,
  envName: string,
  _key: string,
  newValue: string
) =>
  setStore(
    produce((state) => {
      const env = state.environments[envName];
      if (!env) {
        return;
      }

      const existing = env.variables[index];
      if (!existing) {
        throw new Error("Variable missing");
      }
      existing.value = newValue;
    })
  );

export const renameVariable = (
  index: number,
  envName: string,
  newKey: string
) =>
  setStore(
    produce((state) => {
      const env = state.environments[envName];
      if (!env) {
        return;
      }

      const oldVar = env.variables[index];
      const newVar = env.variables.find(({ key }) => key === newKey);
      if (newVar) {
        throw new Error("Overwriting existing variable");
      } else if (!oldVar) {
        throw new Error("Variable missing");
      }
      oldVar.key = newKey;
    })
  );

export const createNewVariable = (
  envName: string,
  key: string,
  value: string
) =>
  setStore(
    produce((state) => {
      const env = state.environments[envName];
      if (!env) {
        return;
      }

      // create new
      env.variables.push({ key, value, isSecret: false });
    })
  );

export const deleteVariable = (envName: string, name: string) =>
  setStore((state) => {
    const env = state.environments[envName];
    if (!env) {
      throw new Error("Unknown env:" + envName);
    }
    return {
      ...state,
      environments: {
        ...state.environments,
        [envName]: {
          ...env,
          variables: env.variables.filter(({ key }) => key !== name),
        },
      },
    };
  });

export const toggleVariableVisibility = (envName: string, name: string) =>
  setStore(
    produce((state) => {
      const env = state.environments[envName];
      if (!env) {
        return;
      }
      const current = env.variables.find(({ key }) => key === name);
      if (current) {
        current.isSecret = !current?.isSecret;
      }
    })
  );

export const setRecipients = (recipients: Array<string>) =>
  setStore((store) => ({ ...store, recipients }));
