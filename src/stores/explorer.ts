import { createMemo } from "solid-js";
import { createStore } from "solid-js/store";
import type { HeaderEntry, ParseValueResult } from "../ipc/bindings.generated";
import { type RequestBodyType } from "../views/explorer/RequestBody";

export type ExplorerStore = {
  uri: string;
  parsedUri: ParseValueResult;
  method: string;
  body: RequestBodyType;
  headers: Array<HeaderEntry>;
  activeEnvName: string | null;
};

const [store, setStore] = createStore<ExplorerStore>({
  uri: "http://localhost:1420/src/App.tsx?test=abc&extended={{gen(UUID)}}&abc={{ABC}}&my_var={{MY_VAR}}",
  parsedUri: { parts: [] },
  method: "GET",
  body: { filePath: null, text: null },
  headers: [{ name: "", value: "" }],
  activeEnvName: null,
});

export const method = createMemo(() => store.method);
export const setMethod = (method: string) =>
  setStore((store) => ({ ...store, method }));

export const uri = createMemo(() => store.uri);
export const setUri = (uri: string) => setStore((store) => ({ ...store, uri }));
export const parsedUri = createMemo(() => store.parsedUri);
export const parsedUriIsValid = createMemo(
  () =>
    store.parsedUri.parts.filter(
      (p) =>
        p.component.type === "variable" && p.component.resolvedValue === null
    ).length > 0
);
export const setParsedUri = (parsedUri: ParseValueResult) =>
  setStore((store) => ({ ...store, parsedUri }));

export const body = createMemo(() => store.body);
export const setBody = (body: RequestBodyType) =>
  setStore((store) => ({ ...store, body }));

export const headers = createMemo(() => store.headers);
export const setHeaders = (headers: Array<HeaderEntry>) =>
  setStore((store) => ({ ...store, headers }));

export const activeEnvName = createMemo(() => store.activeEnvName);
export const setActiveEnvName = (activeEnvName: string | null) =>
  setStore((store) => ({ ...store, activeEnvName }));
