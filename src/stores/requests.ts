import { createStore } from "solid-js/store";
import type { SendRequestResult } from "../ipc/bindings.generated";

export type RequestStore = {
  history: Array<SendRequestResult>;
  activeRequestIndex: null | number;
  isSending: boolean;
};

const [store, setStore] = createStore<RequestStore>({
  history: [],
  activeRequestIndex: null,
  isSending: false,
});

export const addRequest = (request: SendRequestResult) => {
  setStore((state) => ({
    ...state,
    history: [request, ...state.history],
    activeRequestIndex: 0,
  }));
};

export const recentRequests = () => store.history.slice(0, 5);

export const allRequests = () => store.history;

export const activeRequest = () => {
  const index = store.activeRequestIndex;
  if (index === null) {
    return null;
  }
  return store.history[index] || null;
};

export const activeRequestIndex = () => store.activeRequestIndex;

export const setActiveRequestIndex = (index: number) =>
  setStore((state) => ({ ...state, activeRequestIndex: index }));

export const setIsSending = (newValue: boolean) => {
  setStore((state) => ({ ...state, isSending: newValue }));
};

export const isSending = () => store.isSending;
