import { Select as KSelect } from "@kobalte/core";
import { type ParentComponent } from "solid-js";
import { inputGray } from "../utils/styling";
import { chevronDown, Icon } from "./Icon";

const emptyKey = "empty-key";

export const Select: ParentComponent<{
  id: string;
  value?: string;
  label?: string;
  options: Array<{ key: string; label: string }>;
  onChange: (newKey: string | null) => void;
  emptyLabel?: string;
}> = (props) => (
  <KSelect.Root<{ key: string; label: string }>
    disabled={props.options.length === 0}
    options={
      props.emptyLabel
        ? [{ key: emptyKey, label: props.emptyLabel }, ...props.options]
        : props.options
    }
    optionTextValue="label"
    optionValue="key"
    onChange={(newValue) =>
      props.onChange(newValue.key === emptyKey ? null : newValue.key)
    }
    value={
      props.options.find((k) => k.key === props.value) || {
        key: emptyKey,
        label: props.emptyLabel || "-",
      }
    }
    itemComponent={(selection) => (
      <KSelect.Item
        item={selection.item}
        class="px-3 py-2 text-gray-700 hover:text-black"
      >
        <KSelect.ItemLabel>
          {selection.item?.rawValue.label || props.emptyLabel}
        </KSelect.ItemLabel>
      </KSelect.Item>
    )}
  >
    <KSelect.Trigger aria-label={props.label}>
      <div
        class={`text-sm rounded-lg block h-full gap-2 px-3 py-2 ${inputGray} tracking-tight flex items-center`}
      >
        <KSelect.Value<{ key: string; label: string }>>
          {(selection) => selection.selectedOption().label}
        </KSelect.Value>
        <KSelect.Icon>
          <Icon path={chevronDown} />
        </KSelect.Icon>
      </div>
    </KSelect.Trigger>
    <KSelect.Portal>
      <KSelect.Content
        class={`${inputGray} rounded-lg text-sm tracking-tight cursor-pointer shadow-md`}
      >
        <KSelect.Listbox />
      </KSelect.Content>
    </KSelect.Portal>
  </KSelect.Root>
);
