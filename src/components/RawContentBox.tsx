import { type ParentComponent } from "solid-js";

export const RawContentBox: ParentComponent<{ content: string }> = (props) => (
  <div class="text-sm break-all max-h-64 overflow-auto rounded p-2 bg-gray-100">
    {props.content}
  </div>
);
