import { Button } from "@kobalte/core";
import { A, useLocation } from "@solidjs/router";
import {
  createMemo,
  createSignal,
  For,
  JSX,
  type ParentComponent,
  Show,
} from "solid-js";
import { routes } from "../App";
import { loadedWorkspace, setRecipients } from "../stores/workspace";
import { inputBaseStyle } from "../utils/styling";
import { Dialog } from "./Dialog";
import { Heading } from "./Heading";
import { Icon, cog } from "./Icon";
import { Column } from "./Layout";
import { Logo } from "./Logo";
import { Tabs } from "./Tabs";
import { UpdateRecipientsForm } from "../views/settings/UpdateRecipientsForm";

const horizontalLayout = "mx-auto max-w-8xl px-4";

export const Page: ParentComponent<{ navigationActions?: JSX.Element }> = (
  props
) => {
  const location = useLocation();
  const pathname = createMemo(() => location.pathname);
  const currentPage = createMemo(
    () => routes.find(({ path }) => path === pathname()) || null
  );
  const [showSettings, setShowSettings] = createSignal(false);

  return (
    <div class="min-h-full">
      <nav class="bg-gray-800">
        <div class={`${horizontalLayout}`}>
          <div class="flex h-16 items-center justify-between">
            <div class="flex items-center">
              <div class="flex-shrink-0">
                <Logo />
              </div>
              <div class="ml-4 flex items-baseline gap-2">
                <For each={routes}>
                  {({ path, header }) => (
                    <A
                      end
                      href={path}
                      class="px-3 py-2 rounded-md text-sm font-medium"
                      activeClass="bg-gray-900 text-white"
                      inactiveClass="text-gray-300 hover:bg-gray-700 hover:text-white"
                      aria-current={
                        location.pathname === path ? "page" : undefined
                      }
                    >
                      {header}
                    </A>
                  )}
                </For>
              </div>
            </div>
            <div>
              <Dialog
                title="Settings"
                onCloseDialog={() => setShowSettings(false)}
                showDialog={showSettings()}
              >
                <Tabs
                  tabs={[
                    {
                      title: "Workspace",
                      key: "workspace",
                      element: () => (
                        <Column gap="xl">
                          <Column gap="lg">
                            <Heading level="4">Details</Heading>
                            <div>
                              {loadedWorkspace().name} loaded from{" "}
                              {loadedWorkspace().fileName}
                            </div>
                            <div>{loadedWorkspace().description}</div>
                          </Column>
                          <div>
                            <UpdateRecipientsForm
                              recipients={loadedWorkspace().recipients}
                              onUpdate={({ recipients }) => {
                                setRecipients(recipients.filter((r) => !!r));
                              }}
                            />
                          </div>
                        </Column>
                      ),
                    },
                    {
                      title: "Proxy",
                      key: "proxy",
                      element: () => (
                        <Column gap="xl">
                          <Heading level="4">Connection Settings</Heading>
                        </Column>
                      ),
                    },
                  ]}
                />
              </Dialog>
              <Button.Root
                onClick={() => setShowSettings(true)}
                class={`${inputBaseStyle} text-gray-300 hover:text-white hover:bg-gray-700`}
              >
                <Icon size="lg" path={cog} />
              </Button.Root>
            </div>
          </div>
        </div>
      </nav>

      <header class="bg-white shadow">
        <div class={`${horizontalLayout} py-5`}>
          <div class="flex justify-between items-center">
            <Show when={currentPage()} keyed>
              {({ header }) => <Heading level="1">{header}</Heading>}
            </Show>
            {props.navigationActions}
          </div>
        </div>
      </header>
      <main>
        <div class={`${horizontalLayout}`}>
          <div class="py-4">{props.children}</div>
        </div>
      </main>
    </div>
  );
};
