import { mergeProps, type ParentComponent } from "solid-js";

export const Tag: ParentComponent<{ type?: "info" | "error" }> = (props) => {
  const merged = mergeProps({ type: "info" }, props);
  return (
    <div
      class="text-sm bg-white border rounded px-2 py-1 flex items-center gap-1"
      classList={{
        "border-red-300 text-red-600": merged.type === "error",
        "border-gray-200": merged.type === "info",
      }}
    >
      {props.children}
    </div>
  );
};

export const TagGroup: ParentComponent = (props) => (
  <div class="flex gap-2">{props.children}</div>
);
