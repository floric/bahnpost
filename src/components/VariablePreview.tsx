import { Button, Tooltip } from "@kobalte/core";
import {
  type Component,
  createResource,
  createSignal,
  For,
  Match,
  type Setter,
  Switch,
} from "solid-js";
import { JSX } from "solid-js/web/types/jsx";
import { parseValue, type ParseValueResult } from "../ipc/bindings.generated";
import { inputBaseStyle, inputGray } from "../utils/styling";
import { Icon, variable } from "./Icon";
import { activeEnvName } from "../stores/explorer";

export const VariablePreview: Component<{
  id: string;
  value: string;
  children: (setMode: Setter<"edit" | "view">) => JSX.Element;
}> = (props) => {
  const [mode, setMode] = createSignal<"edit" | "view">("view");
  const [uriComponents] = createResource<
    ParseValueResult,
    { value: string; activeEnv: string | null }
  >(
    () => ({ value: props.value, activeEnv: activeEnvName() }),
    ({ value, activeEnv }) => parseValue(value, activeEnv)
  );
  const handleOnEdit = () => {
    setMode("edit");
    const elem = document.getElementById(props.id);
    elem?.focus();
  };
  return (
    <Switch>
      <Match when={mode() === "edit"}>{props.children(setMode)}</Match>
      <Match when={mode() === "view"}>
        <Button.Root
          class={`${inputBaseStyle} ${inputGray} w-full`}
          onClick={handleOnEdit}
        >
          <div class="truncate flex-grow text-left">
            <For each={uriComponents()?.parts}>
              {({ value, component }) => (
                <Switch>
                  <Match
                    when={component.type === "computed" && component}
                    keyed
                  >
                    {(v) => (
                      <span class="p-1 rounded-lg tracking-tight font-bold font-mono bg-blue-300">
                        {v.mode}
                      </span>
                    )}
                  </Match>
                  <Match when={component.type === "literal"}>{value}</Match>
                  <Match
                    when={component.type === "variable" && component}
                    keyed
                  >
                    {(v) => (
                      <Switch>
                        <Match when={v.resolvedValue !== null}>
                          <Tooltip.Root placement="top">
                            <Tooltip.Trigger>
                              <span class="p-1 rounded-lg tracking-tight font-bold font-mono bg-green-300">
                                {v.identifier}
                              </span>
                            </Tooltip.Trigger>
                            <Tooltip.Portal>
                              <Tooltip.Content>
                                <div class="bg-white shadow rounded px-2 py-1 m-1 text-sm">
                                  {v.resolvedValue}
                                </div>
                              </Tooltip.Content>
                            </Tooltip.Portal>
                          </Tooltip.Root>
                        </Match>
                        <Match when={v.resolvedValue === null}>
                          <span class="p-1 rounded-lg tracking-tight font-bold font-mono bg-red-300">
                            {v.identifier}
                          </span>
                        </Match>
                      </Switch>
                    )}
                  </Match>
                </Switch>
              )}
            </For>
          </div>
          <div>
            <Icon path={variable} />
          </div>
        </Button.Root>
      </Match>
    </Switch>
  );
};
