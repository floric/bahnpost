import { For, JSX, Show } from "solid-js";

export type TableItem<T> = {
  data: T;
  cells: Array<{
    value: JSX.Element;
    wordBreak?: "normal" | "never" | "always";
  }>;
};

export const Table = <T,>(props: {
  headers: Array<string>;
  values: Array<TableItem<T>>;
  actions?: Array<(data: T, i: number) => JSX.Element>;
}) => (
  <div class="relative overflow-x-auto rounded border border-gray-200">
    <table class="w-full text-sm text-left table-fixed">
      <thead class="text-xs text-gray-700 uppercase bg-gray-100">
        <tr>
          <For each={props.headers}>
            {(name) => (
              <th scope="col" class="px-4 py-3">
                {name}
              </th>
            )}
          </For>
          <Show when={props.actions}>
            <th scope="col" class="px-4 py-3 text-right w-32">
              Actions
            </th>
          </Show>
        </tr>
      </thead>
      <tbody>
        <For each={props.values}>
          {(line, i) => (
            <tr
              class="bg-white border-b hover:bg-gray-50"
              classList={{ "border-b": i() !== props.values.length - 1 }}
            >
              <For each={line.cells}>
                {({ value, wordBreak }) => (
                  <td
                    class="p-2"
                    classList={{
                      "break-all": wordBreak === "always",
                      "whitespace-nowrap": wordBreak === "never",
                    }}
                  >
                    {value}
                  </td>
                )}
              </For>
              <Show when={props.actions}>
                <td class="p-2 flex justify-end gap-2">
                  {
                    <For each={props.actions || []}>
                      {(action) => action(line.data, i())}
                    </For>
                  }
                </td>
              </Show>
            </tr>
          )}
        </For>
      </tbody>
    </table>
  </div>
);
