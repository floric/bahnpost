import { type ParentComponent } from "solid-js";

export const Card: ParentComponent = (props) => (
  <div class="rounded-lg bg-white border border-gray-300 p-4">
    {props.children}
  </div>
);
