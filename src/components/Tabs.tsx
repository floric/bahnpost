import { Tabs as KTabs } from "@kobalte/core";
import { type Component, createSignal, For, JSX, type Setter } from "solid-js";

export const Tabs: Component<{
  isHeading?: boolean;
  tabs: Array<{
    title: string;
    key: string;
    element: (setSelectedTab: Setter<string>) => JSX.Element;
  }>;
  setTab?: (key: string) => void;
  sideActions?: JSX.Element;
}> = (props) => {
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion, solid/reactivity
  const initialTab = props.tabs[0]!.key;
  const [selectedTab, setSelectedTab] = createSignal(initialTab);
  return (
    <KTabs.Root value={selectedTab()} onChange={setSelectedTab}>
      <div class="flex justify-between items-start">
        <KTabs.List class="flex gap-2">
          <For each={props.tabs}>
            {({ key, title }) => (
              <KTabs.Trigger
                class="rounded-t text-sm hover:bg-gray-100 px-3 py-2 cursor-pointer border-b-2 border-indigo-200 aria-selected:border-b-2 aria-selected:border-indigo-600"
                classList={{
                  "text-xl font-light tracking-tight": props.isHeading,
                }}
                value={key}
              >
                {title}
              </KTabs.Trigger>
            )}
          </For>
        </KTabs.List>
        {props.sideActions}
      </div>
      <div class="mt-4">
        <For each={props.tabs}>
          {({ element, key }) => (
            <KTabs.Content value={key}>{element(setSelectedTab)}</KTabs.Content>
          )}
        </For>
      </div>
    </KTabs.Root>
  );
};
