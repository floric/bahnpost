import { TextField } from "@kobalte/core";
import { FieldValues, type FieldStore, FieldPath } from "@modular-forms/solid";
import { type ParentComponent, Show } from "solid-js";
import { inputBaseStyle, inputError, inputGray } from "../utils/styling";
import { TextFieldInputProps } from "@kobalte/core/dist/types/text-field";

export const FormLabel: ParentComponent<{ key: string }> = (props) => (
  <TextField.Label class="text-sm font-light uppercase" for={props.key}>
    {props.children}
  </TextField.Label>
);

export const FormError = <
  T extends FieldValues,
  N extends FieldPath<T>
>(props: {
  field: FieldStore<T, N>;
}) => (
  <Show when={props.field.error}>
    <TextField.ErrorMessage class="text-sm text-red-600 mt-1">
      {props.field.error}
    </TextField.ErrorMessage>
  </Show>
);

export const FormInput = <T extends FieldValues, N extends FieldPath<T>>(
  props: { field: FieldStore<T, N> } & TextFieldInputProps
) => (
  <TextField.Input
    id={props.field.name}
    class={`${inputBaseStyle} flex-grow`}
    classList={{
      [`${inputGray}`]: !props.field.error,
      [`${inputError}`]: !!props.field.error,
    }}
    autocomplete="off"
    autocapitalize="off"
    spellcheck={false}
    {...(props.field as Record<string, unknown>)}
    {...props}
  />
);
