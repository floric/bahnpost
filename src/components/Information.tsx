import { type ParentComponent } from "solid-js";
import { Centered } from "./Layout";

export const Information: ParentComponent = (props) => (
  <Centered gap="lg">
    <div class="my-10 text-xl font-light">{props.children}</div>
  </Centered>
);
