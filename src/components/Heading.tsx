import { Match, type ParentComponent, Switch } from "solid-js";

export const Heading: ParentComponent<{ level: "1" | "2" | "3" | "4" }> = (
  props
) => (
  <Switch>
    <Match when={props.level === "1"}>
      <h1 class="text-3xl font-bold tracking-tight">{props.children}</h1>
    </Match>
    <Match when={props.level === "2"}>
      <h2 class="text-2xl font-bold tracking-tight">{props.children}</h2>
    </Match>
    <Match when={props.level === "3"}>
      <h3 class="text-xl font-light tracking-tight">{props.children}</h3>
    </Match>
    <Match when={props.level === "4"}>
      <h4 class="font-bold">{props.children}</h4>
    </Match>
  </Switch>
);
