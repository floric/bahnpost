import { Dialog as KDialog } from "@kobalte/core";
import { type ParentComponent } from "solid-js";
import { inputBaseStyle, inputGray } from "../utils/styling";
import { Heading } from "./Heading";
import { Icon, xMark } from "./Icon";

export const Dialog: ParentComponent<{
  title: string;
  showDialog: boolean;
  onCloseDialog: () => void;
}> = (props) => (
  <KDialog.Root open={props.showDialog} modal>
    <KDialog.Portal>
      <KDialog.Overlay class="fixed top-0 left-0 w-full h-full flex justify-center items-center bg-opacity-50 bg-black backdrop-blur-sm z-50">
        <KDialog.Content class="bg-white rounded-md p-4 border border-gray-200 shadow-lg min-w-xl max-w-4xl min-h-md">
          <div class="flex justify-between items-center mb-4 gap-8">
            <KDialog.Title>
              <Heading level="2">{props.title}</Heading>
            </KDialog.Title>
            <KDialog.CloseButton
              class={`${inputBaseStyle} ${inputGray}`}
              onClick={props.onCloseDialog}
            >
              <Icon path={xMark} />
            </KDialog.CloseButton>
          </div>
          <KDialog.Description>{props.children}</KDialog.Description>
        </KDialog.Content>
      </KDialog.Overlay>
    </KDialog.Portal>
  </KDialog.Root>
);
