import { type Component } from "solid-js";

export const Logo: Component = () => (
  <div class="bg-indigo-600 text-gray-50 px-2 py-1 flex justify-center items-center rounded">
    <div class="tracking-tight font-bold text-lg">myzel</div>
  </div>
);
