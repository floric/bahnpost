import { type ParentComponent } from "solid-js";

export const Column: ParentComponent<{
  gap: "sm" | "md" | "lg" | "xl";
}> = (props) => (
  <div
    class="flex flex-col"
    classList={{
      "gap-1": props.gap === "sm",
      "gap-2": props.gap === "md",
      "gap-4": props.gap === "lg",
      "gap-6": props.gap === "xl",
    }}
  >
    {props.children}
  </div>
);

export const Row: ParentComponent<{ gap: "md" | "lg" }> = (props) => (
  <div
    class="flex flex-row items-center"
    classList={{ "gap-2": props.gap === "md", "gap-4": props.gap === "lg" }}
  >
    {props.children}
  </div>
);

export const Centered: ParentComponent<{ gap?: "md" | "lg" }> = (props) => (
  <div
    class="flex flex-col justify-center items-center"
    classList={{ "gap-2": props.gap === "md", "gap-4": props.gap === "lg" }}
  >
    {props.children}
  </div>
);
