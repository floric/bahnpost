import { type Component, createMemo, Match, Show, Switch } from "solid-js";
import { Heading } from "../../components/Heading";
import { TagGroup } from "../../components/Tag";
import { Column } from "../../components/Layout";
import { RawContentBox } from "../../components/RawContentBox";
import { Table } from "../../components/Table";
import { DurationTag } from "./tags/DurationTag";
import { ErrorTag } from "./tags/ErrorTag";
import { SizeTag } from "./tags/SizeTag";
import { StatusTag } from "./tags/StatusTag";
import { EnvTag } from "./tags/EnvTag";
import type { SendRequestResult } from "../../ipc/bindings.generated";

export const RequestSummary: Component<{
  result: SendRequestResult | null;
}> = (props) => {
  const request = createMemo(() => props.result?.requestData || null);
  return (
    <Show when={request()} keyed>
      {(req) => (
        <Column gap="xl">
          <Show when={props.result} keyed>
            {(result) => (
              <>
                <Show
                  when={result.response.type === "success" && result.response}
                  keyed
                >
                  {(success) => (
                    <TagGroup>
                      <StatusTag success={success} />
                      <SizeTag success={success} />
                      <DurationTag success={success} />
                      <EnvTag requestData={result.requestData} />
                    </TagGroup>
                  )}
                </Show>
                <Show
                  when={result.response.type === "error" && result.response}
                  keyed
                >
                  {(error) => (
                    <TagGroup>
                      <ErrorTag error={error} />
                    </TagGroup>
                  )}
                </Show>
                <div class="tracking-tight text-sm">
                  <span class="font-bold">{req.method}</span>{" "}
                  {result.resolvedData.uri}
                </div>
              </>
            )}
          </Show>
          <Column gap="md">
            <Heading level="4">Request Headers</Heading>
            <Switch>
              <Match when={req.headers.length}>
                <Table
                  headers={["Key", "Value"]}
                  values={req.headers.map((header) => ({
                    cells: [{ value: header.name }, { value: header.value }],
                    data: header,
                  }))}
                />
              </Match>
              <Match when={!req.headers.length}>
                <div class="text-sm">No Headers sent.</div>
              </Match>
            </Switch>
          </Column>
          <Column gap="md">
            <Heading level="4">Request Body</Heading>
            <Switch>
              <Match when={req.body}>
                <RawContentBox content={req.body || ""} />
              </Match>
              <Match when={!req.body}>
                <div class="text-sm">No Body sent.</div>
              </Match>
            </Switch>
          </Column>
        </Column>
      )}
    </Show>
  );
};
