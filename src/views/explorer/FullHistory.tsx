import { Button } from "@kobalte/core";
import { format } from "date-fns";
import { type Component, createMemo, For, Match, Switch } from "solid-js";
import { Heading } from "../../components/Heading";
import { arrowSmallRight, Icon } from "../../components/Icon";
import { Column } from "../../components/Layout";
import type { SendRequestResult } from "../../ipc/bindings.generated";
import { allRequests, setActiveRequestIndex } from "../../stores/requests";
import { inputBaseStyle, inputGray } from "../../utils/styling";

export const FullHistory: Component<{ closeDialog: () => void }> = (props) => {
  const groupedHistory = createMemo(() => {
    const all = allRequests();
    const groups: Record<
      string,
      Array<{ i: number; result: SendRequestResult }>
    > = {};
    for (let i = 0; i < all.length; i++) {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const result = all[i]!;
      const date = format(new Date(result.dateTime), "MM/dd/yyyy");
      const existingGroup = groups[date] || null;
      if (!existingGroup) {
        groups[date] = [{ i, result }];
      } else {
        groups[date] = [{ i, result }, ...existingGroup];
      }
    }
    for (const group of Object.values(groups)) {
      group.sort(({ result: a }, { result: b }) => {
        return new Date(b.dateTime).getTime() - new Date(a.dateTime).getTime();
      });
    }
    return groups;
  });
  return (
    <Column gap="xl">
      <For each={Object.entries(groupedHistory())}>
        {([date, requests]) => (
          <Column gap="lg">
            <Heading level="3">{date}</Heading>
            <Column gap="md">
              <For each={requests}>
                {({ i, result }) => {
                  const handleShowDetails = () => {
                    setActiveRequestIndex(i);
                    props.closeDialog();
                  };
                  return (
                    <div class="flex justify-between items-center gap-4">
                      <div>
                        <span class="font-bold">
                          {result.requestData.method}
                        </span>{" "}
                        {result.requestData.uri} |{" "}
                        <Switch>
                          <Match
                            when={
                              result.response.type === "success" &&
                              result.response
                            }
                            keyed
                          >
                            {(success) => (
                              <>
                                {success.status} {success.statusText}
                              </>
                            )}
                          </Match>
                        </Switch>
                      </div>
                      <div>
                        <Button.Root
                          class={`${inputBaseStyle} ${inputGray}`}
                          onClick={handleShowDetails}
                        >
                          <Icon size="sm" path={arrowSmallRight} />
                        </Button.Root>
                      </div>
                    </div>
                  );
                }}
              </For>
            </Column>
          </Column>
        )}
      </For>
    </Column>
  );
};
