import { Button } from "@kobalte/core";
import { createSignal, type ParentComponent, Show } from "solid-js";
import { Card } from "../../components/Card";
import { Dialog } from "../../components/Dialog";
import { Icon, clock, arrowUTurnLeft } from "../../components/Icon";
import { Column, Centered } from "../../components/Layout";
import { Select } from "../../components/Select";
import { Spinner } from "../../components/Spinner";
import { Tabs } from "../../components/Tabs";
import {
  activeRequest,
  activeRequestIndex,
  isSending,
  recentRequests,
  setActiveRequestIndex,
} from "../../stores/requests";
import { formatDatetime } from "../../utils/formatting";
import { inputBaseStyle, inputGray } from "../../utils/styling";
import { FullHistory } from "./FullHistory";
import { RequestSummary } from "./RequestSummary";
import { ResponseBody } from "./ResponseBody";
import { ResponseError } from "./ResponseError";
import { ResponseHeaders } from "./ResponseHeaders";
import { Information } from "../../components/Information";
import type { SendRequestResult } from "../../ipc/bindings.generated";

export const Responses: ParentComponent = () => {
  const [showFullHistory, setShowFullHistory] = createSignal(false);
  const handleShowFullHistory = () => setShowFullHistory(true);
  return (
    <Column gap="md">
      <Show when={recentRequests().length === 0 && !isSending()}>
        <Card>
          <Information>No Requests sent yet.</Information>
        </Card>
      </Show>
      <Show when={activeRequest() !== null && !isSending()}>
        <div class="flex gap-2">
          <Select
            id="history-item"
            onChange={(val) => setActiveRequestIndex(Number(val))}
            value={activeRequestIndex()?.toString() || undefined}
            options={recentRequests().map(mapToHistoryItem)}
          />
          <Dialog
            title="History"
            onCloseDialog={() => setShowFullHistory(false)}
            showDialog={showFullHistory()}
          >
            <FullHistory closeDialog={() => setShowFullHistory(false)} />
          </Dialog>
          <Button.Root
            class={`${inputBaseStyle} ${inputGray}`}
            onClick={handleShowFullHistory}
          >
            <Icon path={clock} /> Full History
          </Button.Root>
        </div>
      </Show>
      <Show when={isSending()}>
        <Card>
          <Centered gap="lg">
            <Spinner />
            <div>Request in Progress</div>
          </Centered>
        </Card>
      </Show>
      <Show when={!isSending() && activeRequest()} keyed>
        {(result) => (
          <Column gap="md">
            <Card>
              <Column gap="lg">
                <Tabs
                  tabs={[
                    {
                      title: "Summary",
                      key: "request",
                      element: () => <RequestSummary result={result} />,
                    },
                    ...(() => {
                      const response = result.response;
                      if (response.type === "success") {
                        return [
                          {
                            title: "Headers",
                            key: "headers",
                            element: () => (
                              <ResponseHeaders
                                headers={response.headers || []}
                              />
                            ),
                          },
                          {
                            title: "Body",
                            key: "body",
                            element: () => (
                              <ResponseBody body={response.body || ""} />
                            ),
                          },
                        ];
                      } else if (response.type === "error") {
                        return [
                          {
                            title: "Error",
                            key: "error",
                            element: () => <ResponseError error={response} />,
                          },
                        ];
                      } else {
                        return [];
                      }
                    })(),
                    {
                      title: "Timing",
                      key: "timing",
                      element: () => <div>TODO</div>,
                    },
                  ]}
                  sideActions={
                    <div class="flex gap-2">
                      <Button.Root class={`${inputBaseStyle} ${inputGray}`}>
                        <Icon path={arrowUTurnLeft} /> Send again
                      </Button.Root>
                    </div>
                  }
                />
              </Column>
            </Card>
          </Column>
        )}
      </Show>
    </Column>
  );
};

const mapToHistoryItem = (
  result: SendRequestResult,
  i: number
): { key: string; label: string } => {
  const requestData = result.requestData;
  return {
    key: i.toString(),
    label: `${formatDatetime(result.dateTime || "")}
                 | 
                ${requestData.method} 
                ${new URL(requestData.uri).host}`,
  };
};
