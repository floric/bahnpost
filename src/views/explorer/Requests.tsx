import { Button } from "@kobalte/core";
import {
  createEffect,
  createResource,
  createSignal,
  type ParentComponent,
  type Setter,
} from "solid-js";
import { Card } from "../../components/Card";
import { Column } from "../../components/Layout";
import { Select } from "../../components/Select";
import { Tabs } from "../../components/Tabs";
import {
  parseValue,
  sendRequest,
  type SendRequestResult,
} from "../../ipc/bindings.generated";
import {
  setMethod,
  method,
  setUri,
  uri,
  body,
  headers,
  setHeaders,
  setBody,
  activeEnvName,
  setParsedUri,
  parsedUriIsValid,
} from "../../stores/explorer";
import { addRequest, isSending, setIsSending } from "../../stores/requests";
import { handleError } from "../../utils/errorhandling";
import { inputBaseStyle, inputPrimary } from "../../utils/styling";
import { QueryParams } from "./QueryParams";
import { RequestBody } from "./RequestBody";
import { RequestHeaders } from "./RequestHeaders";
import { URLInput } from "./URLInput";

const supportedMethods = ["GET", "POST", "PUT", "DELETE", "PATCH"];

export const Requests: ParentComponent<{
  setSelectedTab: Setter<string>;
}> = (props) => {
  const start = Array.from(new URL(uri()).searchParams.entries()).map(
    ([name, value]) => ({
      name,
      value,
    })
  );
  const [parameters, setParameters] =
    createSignal<Array<{ name: string; value: string }>>(start);
  const [shouldSendRequest, setShouldSendRequest] = createSignal(false);
  const [, { refetch }] = createResource<SendRequestResult | null>(async () => {
    if (!shouldSendRequest()) {
      return null;
    }
    try {
      setIsSending(true);
      const response = await sendRequest({
        uri: uri(),
        method: method(),
        headers: headers().filter((h) => h.name),
        // TODO support more body types
        body: body().text,
        activeEnvName: activeEnvName(),
      });
      addRequest(response);
      props.setSelectedTab("response");
      setIsSending(false);
      return response;
    } catch (err) {
      handleError(err);
    }

    setShouldSendRequest(false);
    setIsSending(false);

    return null;
  });
  const handleSendRequest = () => {
    setShouldSendRequest(true);
    refetch();
  };
  createEffect(() => {
    const updatedUri = uri();
    const updateEnv = activeEnvName();

    const updateParsedUri = async () => {
      const result = await parseValue(updatedUri, updateEnv);
      setParsedUri(result);
    };

    updateParsedUri();
  });
  return (
    <Column gap="md">
      <div class="flex flex-row gap-2">
        <Select
          id="method"
          onChange={(newValue) => setMethod(newValue || "GET")}
          value={method()}
          options={supportedMethods.map((m) => ({ key: m, label: m }))}
        />
        <div class="flex-grow min-w-0 max-w-4xl">
          <URLInput
            uri={uri()}
            onChange={(newUri) => {
              try {
                const parsedUri = new URL(newUri);
                const newParameters = Array.from(parsedUri.searchParams).map(
                  ([name, value]) => ({ name, value })
                );
                setUri(newUri);
                setParameters(newParameters);
              } catch (err) {
                handleError("Invalid URI format");
              }
            }}
          />
        </div>
        <div>
          <Button.Root
            class={`${inputBaseStyle} ${inputPrimary}`}
            disabled={isSending() || parsedUriIsValid()}
            onClick={handleSendRequest}
          >
            Send
          </Button.Root>
        </div>
      </div>
      <Card>
        <Tabs
          tabs={[
            {
              title: "Query Params",
              key: "query",
              element: () => (
                <QueryParams
                  parameters={parameters()}
                  setParameters={(newParameters) => {
                    // slightly hacky workaround to set new parameters conviently
                    const url = new URL(uri());
                    // clear
                    Array.from(url.searchParams.keys()).forEach((k) =>
                      url.searchParams.delete(k)
                    );
                    // fill again
                    newParameters
                      .filter(({ name }) => !!name)
                      .forEach(({ name, value }) =>
                        url.searchParams.append(name, value)
                      );
                    setUri(decodeURI(url.toString()));
                    setParameters(newParameters);
                  }}
                />
              ),
            },
            {
              title: "Headers",
              key: "headers",
              element: () => (
                <RequestHeaders headers={headers()} setHeaders={setHeaders} />
              ),
            },
            {
              title: "Body",
              key: "body",
              element: () => <RequestBody body={body()} setBody={setBody} />,
            },
          ]}
        />
      </Card>
    </Column>
  );
};
