import { type Component, Show } from "solid-js";
import { Heading } from "../../components/Heading";
import { Column } from "../../components/Layout";
import type { SendRequestResponse } from "../../ipc/bindings.generated";

export const ResponseError: Component<{
  error: SendRequestResponse & { type: "error" };
}> = (props) => (
  <Column gap="md">
    <Heading level="4">{props.error.message}</Heading>
    <Show when={props.error.details} keyed>
      {(details) => <div>{details}</div>}
    </Show>
  </Column>
);
