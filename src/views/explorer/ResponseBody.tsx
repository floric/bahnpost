import { Button } from "@kobalte/core";
import { writeClipboard } from "@solid-primitives/clipboard";
import { type Component } from "solid-js";
import { Icon, clipboardDocument, arrowDownTray } from "../../components/Icon";
import { RawContentBox } from "../../components/RawContentBox";
import { inputBaseStyle, inputGray } from "../../utils/styling";

export const ResponseBody: Component<{ body: string }> = (props) => {
  const handleCopyToClipboard = () => {
    writeClipboard(props.body);
  };
  const handleSaveToFile = () => {
    // TODO save to file
  };
  return (
    <>
      <div class="flex gap-2 mb-4">
        <Button.Root
          class={`${inputBaseStyle} ${inputGray}`}
          onClick={handleCopyToClipboard}
        >
          <Icon path={clipboardDocument} />
        </Button.Root>
        <Button.Root
          class={`${inputBaseStyle} ${inputGray}`}
          onClick={handleSaveToFile}
        >
          <Icon path={arrowDownTray} />
        </Button.Root>
      </div>
      <RawContentBox content={props.body} />
    </>
  );
};
