import { type Component } from "solid-js";
import { Tag } from "../../../components/Tag";
import type { SendRequestResponse } from "../../../ipc/bindings.generated";
import { formatDuration } from "../../../utils/formatting";

export const DurationTag: Component<{
  success: SendRequestResponse & { type: "success" };
}> = (props) => (
  <Tag>
    <span class="uppercase">Duration</span> |{" "}
    {formatDuration(props.success.durationMs || 0)}
  </Tag>
);
