import { type Component } from "solid-js";
import { Tag } from "../../../components/Tag";
import type { SendRequestResponse } from "../../../ipc/bindings.generated";
import { formatFileSize } from "../../../utils/formatting";

export const SizeTag: Component<{
  success: SendRequestResponse & { type: "success" };
}> = (props) => (
  <Tag>
    <span class="uppercase">Size</span> |{" "}
    {formatFileSize(props.success.bodySize || 0)}
  </Tag>
);
