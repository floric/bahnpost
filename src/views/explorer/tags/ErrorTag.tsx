import { type Component } from "solid-js";
import { Tag } from "../../../components/Tag";
import type { SendRequestResponse } from "../../../ipc/bindings.generated";

export const ErrorTag: Component<{
  error: SendRequestResponse & { type: "error" };
}> = (props) => (
  <Tag type="error">
    <span class="uppercase">Error</span> | {props.error.message}
  </Tag>
);
