import { type Component, Match, Switch } from "solid-js";
import { Tag } from "../../../components/Tag";
import type { SendRequestResponse } from "../../../ipc/bindings.generated";

export const StatusTag: Component<{
  success: SendRequestResponse & { type: "success" };
}> = (props) => (
  <Tag>
    <span class="uppercase">Status</span> | {props.success.status}{" "}
    {props.success.statusText}
    <Switch
      fallback={<div class="w-3 h-3 bg-red-500 rounded-full inline-block" />}
    >
      <Match when={props.success.status < 200}>
        <div class="w-3 h-3 bg-gray-700 rounded-full inline-block" />
      </Match>
      <Match when={props.success.status < 300}>
        <div class="w-3 h-3 bg-green-700 rounded-full inline-block" />
      </Match>
      <Match when={props.success.status < 400}>
        <div class="w-3 h-3 bg-yellow-500 rounded-full inline-block" />
      </Match>
      <Match when={props.success.status < 500}>
        <div class="w-3 h-3 bg-orange-500 rounded-full inline-block" />
      </Match>
    </Switch>
  </Tag>
);
