import { type Component } from "solid-js";
import { Tag } from "../../../components/Tag";
import type { RequestData } from "../../../ipc/bindings.generated";

export const EnvTag: Component<{ requestData: RequestData }> = (props) => (
  <Tag>
    <span class="uppercase">Env</span> |{" "}
    {props.requestData.activeEnvName || "-"}
  </Tag>
);
