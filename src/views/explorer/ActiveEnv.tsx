import { type Component } from "solid-js";
import { Row } from "../../components/Layout";
import { Select } from "../../components/Select";
import { activeEnvName, setActiveEnvName } from "../../stores/explorer";
import { environmentNames } from "../../stores/workspace";

export const ActiveEnv: Component = () => {
  return (
    <Row gap="md">
      <label class="text-sm font-light uppercase" for="environment">
        Env
      </label>
      <Select
        id="environment"
        emptyLabel="None"
        value={activeEnvName() || undefined}
        onChange={setActiveEnvName}
        options={environmentNames().map((key) => ({ key, label: key }))}
      />
    </Row>
  );
};
