import {
  type Component,
  createEffect,
  createSignal,
  Match,
  Switch,
} from "solid-js";
import { Select } from "../../components/Select";

export type RequestBodyType = { text: string | null; filePath: string | null };

export const RequestBody: Component<{
  body: RequestBodyType;
  setBody: (newBody: RequestBodyType) => void;
}> = (props) => {
  const [bodyType, setBodyType] = createSignal<"none" | "text" | "binaryFile">(
    "none"
  );
  // reset content on type change
  createEffect(() => {
    if (
      bodyType() === "text" ||
      bodyType() === "binaryFile" ||
      bodyType() === "none"
    ) {
      props.setBody({ text: null, filePath: null });
    }
  });
  return (
    <div>
      <div class="flex items-center gap-4">
        <label class="text-sm font-light uppercase" for="bodyType">
          Type
        </label>
        <Select
          id="bodyType"
          onChange={(newValue) =>
            setBodyType(newValue as "none" | "text" | "binaryFile")
          }
          value={bodyType()}
          options={[
            { key: "none", label: "None" },
            { key: "text", label: "Text" },
            { key: "binaryFile", label: "Binary File" },
          ]}
        />
      </div>
      <Switch>
        <Match when={bodyType() === "text"}>
          <textarea
            value={props.body.text || ""}
            class="mt-4 w-full h-64 text-sm rounded p-2 bg-gray-100 break-all"
            onChange={(ev) =>
              props.setBody({
                filePath: null,
                text: ev.currentTarget.value || null,
              })
            }
          />
        </Match>
        <Match when={bodyType() === "binaryFile"}>
          <div>TODO</div>
        </Match>
      </Switch>
    </div>
  );
};
