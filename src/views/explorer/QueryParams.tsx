import { Button, TextField } from "@kobalte/core";
import { type Component, JSX } from "solid-js";
import { Icon, xMark, plus } from "../../components/Icon";
import { Table } from "../../components/Table";
import { VariablePreview } from "../../components/VariablePreview";
import { inputBaseStyle, inputGray } from "../../utils/styling";

export const QueryParams: Component<{
  parameters: Array<{ name: string; value: string }>;
  setParameters: (
    newParameters: Array<{ name: string; value: string }>
  ) => void;
}> = (props) => {
  const handleAddHeader = () =>
    props.setParameters([...props.parameters, { name: "", value: "" }]);
  return (
    <div class="flex flex-col gap-2">
      <Table
        headers={["Name", "Value"]}
        values={props.parameters.map(({ name, value }, i) => {
          const handleOnChangeName: JSX.EventHandlerUnion<
            HTMLInputElement,
            Event
          > = (ev) =>
            props.setParameters([
              ...props.parameters.slice(0, i),
              { name: ev.currentTarget.value, value },
              ...props.parameters.slice(i + 1),
            ]);
          return {
            data: { name, value },
            cells: [
              {
                value: (
                  <TextField.Root>
                    <TextField.Input
                      class={`${inputBaseStyle} ${inputGray} w-full`}
                      onChange={handleOnChangeName}
                      value={name}
                      placeholder="Name"
                      autocomplete="off"
                      autocapitalize="off"
                      spellcheck={false}
                    />
                  </TextField.Root>
                ),
                wordBreak: "never",
              },
              {
                value: (
                  <VariablePreview id={`param-${name}`} value={value}>
                    {(setMode) => (
                      <TextField.Root>
                        <TextField.Input
                          id={`param-${name}`}
                          class={`${inputBaseStyle} ${inputGray} w-full`}
                          onChange={(ev) => {
                            props.setParameters([
                              ...props.parameters.slice(0, i),
                              { name, value: ev.currentTarget.value },
                              ...props.parameters.slice(i + 1),
                            ]);
                            setMode("view");
                          }}
                          onFocusOut={() => setMode("view")}
                          value={value}
                          placeholder="Value"
                          autocomplete="off"
                          autocapitalize="off"
                          spellcheck={false}
                        />
                      </TextField.Root>
                    )}
                  </VariablePreview>
                ),
                wordBreak: "always",
              },
            ],
          };
        })}
        actions={[
          (_, i) => {
            const handleDeleteParam = () =>
              props.setParameters([
                ...props.parameters.slice(0, i),
                ...props.parameters.slice(i + 1),
              ]);
            return (
              <Button.Root
                class={`${inputBaseStyle} ${inputGray}`}
                onClick={handleDeleteParam}
              >
                <Icon path={xMark} />
              </Button.Root>
            );
          },
        ]}
      />
      <Button.Root
        class={`${inputBaseStyle} ${inputGray}`}
        onClick={handleAddHeader}
      >
        <Icon path={plus} />
        Add Header
      </Button.Root>
    </div>
  );
};
