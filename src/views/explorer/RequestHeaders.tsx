import { Button, TextField } from "@kobalte/core";
import { type Component, JSX } from "solid-js";
import { Icon, plus, xMark } from "../../components/Icon";
import { Table } from "../../components/Table";
import { VariablePreview } from "../../components/VariablePreview";
import { inputBaseStyle, inputGray } from "../../utils/styling";

export const RequestHeaders: Component<{
  headers: Array<{ name: string; value: string }>;
  setHeaders: (newHeaders: Array<{ name: string; value: string }>) => void;
}> = (props) => {
  const handleAddHeader = () =>
    props.setHeaders([...props.headers, { name: "", value: "" }]);

  return (
    <div class="flex flex-col gap-2">
      <Table
        headers={["Name", "Value"]}
        values={props.headers.map(({ name, value }, i) => {
          const handleOnChangeName: JSX.EventHandlerUnion<
            HTMLInputElement,
            Event
          > = (ev) =>
            props.setHeaders([
              ...props.headers.slice(0, i),
              { name: ev.currentTarget.value, value },
              ...props.headers.slice(i + 1),
            ]);
          return {
            data: { name, value },
            cells: [
              {
                value: (
                  <TextField.Root>
                    <TextField.Input
                      class={`${inputBaseStyle} ${inputGray} w-full`}
                      onChange={handleOnChangeName}
                      value={name}
                      placeholder="Name"
                      autocomplete="off"
                      autocapitalize="off"
                      spellcheck={false}
                    />
                  </TextField.Root>
                ),
                wordBreak: "never",
              },
              {
                value: (
                  <VariablePreview id={`param-${name}`} value={value}>
                    {(setMode) => (
                      <TextField.Root>
                        <TextField.Input
                          id={`param-${name}`}
                          class={`${inputBaseStyle} ${inputGray} w-full`}
                          onChange={(ev) => {
                            props.setHeaders([
                              ...props.headers.slice(0, i),
                              { name, value: ev.currentTarget.value },
                              ...props.headers.slice(i + 1),
                            ]);
                            setMode("view");
                          }}
                          onFocusOut={() => setMode("view")}
                          value={value}
                          placeholder="Value"
                          autocomplete="off"
                          autocapitalize="off"
                          spellcheck={false}
                        />
                      </TextField.Root>
                    )}
                  </VariablePreview>
                ),
                wordBreak: "always",
              },
            ],
          };
        })}
        actions={[
          (_, i) => {
            const handleRemoveHeader = () =>
              props.setHeaders([
                ...props.headers.slice(0, i),
                ...props.headers.slice(i + 1),
              ]);
            return (
              <Button.Root
                class={`${inputBaseStyle} ${inputGray}`}
                onClick={handleRemoveHeader}
              >
                <Icon path={xMark} />
              </Button.Root>
            );
          },
        ]}
      />
      <Button.Root
        class={`${inputBaseStyle} ${inputGray}`}
        onClick={handleAddHeader}
      >
        <Icon path={plus} />
        Add Header
      </Button.Root>
    </div>
  );
};
