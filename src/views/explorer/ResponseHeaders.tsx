import { Button } from "@kobalte/core";
import { writeClipboard } from "@solid-primitives/clipboard";
import { type Component } from "solid-js";
import { Icon, clipboardDocument } from "../../components/Icon";
import { Table } from "../../components/Table";
import { inputBaseStyle, inputGray } from "../../utils/styling";

export const ResponseHeaders: Component<{
  headers: Array<{ name: string; value: string }>;
}> = (props) => {
  return (
    <Table
      headers={["Name", "Value"]}
      values={props.headers.map(({ name, value }) => ({
        data: { name, value },
        cells: [
          { value: name, wordBreak: "never" },
          { value, wordBreak: "always" },
        ],
      }))}
      actions={[
        (data) => {
          const handleCopyToClipboard = () => writeClipboard(data.value || "");
          return (
            <Button.Root
              class={`${inputBaseStyle} ${inputGray}`}
              onClick={handleCopyToClipboard}
            >
              <Icon path={clipboardDocument} />
            </Button.Root>
          );
        },
      ]}
    />
  );
};
