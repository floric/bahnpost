import { TextField } from "@kobalte/core";
import { type Component, JSX, createSignal } from "solid-js";
import { VariablePreview } from "../../components/VariablePreview";
import { inputBaseStyle, inputError, inputGray } from "../../utils/styling";
import { handleError } from "../../utils/errorhandling";

const urlInputId = "url-input";

export const URLInput: Component<{
  uri: string;
  onChange: (newUri: string) => void;
}> = (props) => {
  const [isValid, setIsValid] = createSignal(true);
  const handleOnChangeUri: JSX.EventHandlerUnion<HTMLInputElement, Event> = (
    ev
  ) => props.onChange(ev.currentTarget.value);
  return (
    <VariablePreview id={urlInputId} value={props.uri}>
      {(setMode) => (
        <TextField.Root>
          <TextField.Input
            id={urlInputId}
            classList={{
              [`${inputBaseStyle} ${inputGray} w-full`]: true,
              [`${inputError}`]: !isValid(),
            }}
            onChange={(ev) => {
              const validationResult = isValidUri(ev.currentTarget.value);
              setIsValid(validationResult);
              if (validationResult) {
                handleOnChangeUri(ev);
                setMode("view");
              }
            }}
            onFocusOut={(ev) => {
              const validationResult = isValidUri(ev.currentTarget.value);
              setIsValid(validationResult);
              if (validationResult) {
                handleOnChangeUri(ev);
                setMode("view");
              }
            }}
            value={props.uri}
            placeholder="URL"
            autocomplete="off"
            autocapitalize="off"
            spellcheck={false}
          />
        </TextField.Root>
      )}
    </VariablePreview>
  );
};

const isValidUri = (uri: string): boolean => {
  try {
    const parsedUri = new URL(uri);
    if (parsedUri.protocol !== "https:" && parsedUri.protocol !== "http:") {
      throw Error("Unsupported protocol");
    }
    if (!Number.isInteger(Number(parsedUri.port))) {
      throw Error("Unsupport port");
    }
    return true;
  } catch (err) {
    handleError("Invalid URI format");
    return false;
  }
};
