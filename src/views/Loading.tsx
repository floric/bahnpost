import { Page } from "../components/Page";

const Loading = () => {
  return <Page>Loading...</Page>;
};

export default Loading;
