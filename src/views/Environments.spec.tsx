import { describe, expect, test } from "vitest";
import { render } from "@solidjs/testing-library";
import Environments from "./Environments";
import { Route, Router } from "@solidjs/router";

describe("<Environments />", () => {
  test("should render without error", () => {
    const { container } = render(() => (
      <Router>
        <Route path="" component={Environments} />
      </Router>
    ));

    expect(container.innerHTML).toBeTruthy();
  });
});
