import { Button, TextField } from "@kobalte/core";
import { type Component } from "solid-js";
import { Column } from "../../components/Layout";
import { inputBaseStyle, inputPrimary } from "../../utils/styling";
import { createForm, required } from "@modular-forms/solid";
import { FormError, FormInput, FormLabel } from "../../components/Form";

type FormContent = { name: string; description: string };

export const EditEnvForm: Component<{
  name: string;
  description: string;
  onUpdate: (form: FormContent) => void;
}> = (props) => {
  const [, { Form, Field }] = createForm<FormContent>({
    initialValues: { name: props.name, description: props.description },
  });
  return (
    <Form onSubmit={props.onUpdate}>
      <Column gap="lg">
        <Field name="name" validate={[required("Name missing")]}>
          {(field, props) => (
            <TextField.Root
              class="flex flex-col"
              validationState={field.error ? "invalid" : "valid"}
            >
              <FormLabel key={field.name}>Name</FormLabel>
              <FormInput field={field} {...props} />
              <FormError field={field} />
            </TextField.Root>
          )}
        </Field>
        <Field name="description" validate={[]}>
          {(field, props) => (
            <TextField.Root
              class="flex flex-col"
              validationState={field.error ? "invalid" : "valid"}
            >
              <FormLabel key={field.name}>Description</FormLabel>
              <FormInput field={field} {...props} />
              <FormError field={field} />
            </TextField.Root>
          )}
        </Field>
        <Button.Root class={`${inputBaseStyle} ${inputPrimary}`} type="submit">
          Update Environment
        </Button.Root>
      </Column>
    </Form>
  );
};
