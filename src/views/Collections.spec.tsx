import { describe, expect, test } from "vitest";
import { render } from "@solidjs/testing-library";
import Collections from "./Collections";
import { Route, Router } from "@solidjs/router";

describe("<Collections />", () => {
  test("should render without error", () => {
    const { container } = render(() => (
      <Router>
        <Route path="" component={Collections} />
      </Router>
    ));

    expect(container.innerHTML).toBeTruthy();
  });
});
