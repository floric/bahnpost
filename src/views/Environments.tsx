import { Button, TextField } from "@kobalte/core";
import { createMemo, createSignal, Show } from "solid-js";
import { Card } from "../components/Card";
import { Dialog } from "../components/Dialog";
import { Heading } from "../components/Heading";
import {
  Icon,
  xMark,
  plus,
  lockClosed,
  lockOpen,
  eyeSlash,
  eye,
  cog,
  trash,
} from "../components/Icon";
import { Information } from "../components/Information";
import { Column, Row } from "../components/Layout";
import { Page } from "../components/Page";
import { Select } from "../components/Select";
import { Table } from "../components/Table";
import { Tabs } from "../components/Tabs";
import { activeEnvName } from "../stores/explorer";
import {
  updateVariable,
  environmentNames,
  environments,
  deleteVariable,
  toggleVariableVisibility,
  addEnvironment,
  createNewVariable,
  renameVariable,
  updateEnvironment,
  deleteEnvironment,
  type Environment,
} from "../stores/workspace";
import {
  inputBaseStyle,
  buttonDanger,
  inputGray,
  inputPrimary,
} from "../utils/styling";
import { CreateEnvForm } from "./environments/CreateEnvForm";
import { EditEnvForm } from "./environments/EditEnvForm";

const Environments = () => {
  const [visibleVariables, setVisibleVariables] = createSignal<
    Record<string, boolean>
  >({});
  const [shownEnvName, setShownEnvName] = createSignal<string | null>(
    activeEnvName()
  );
  const currentEnvironment = createMemo<{
    name: string;
    content: Environment;
  } | null>(() => {
    const envName = shownEnvName();
    if (!envName) {
      return null;
    }
    const env = environments().environments[envName] || null;
    return env !== null ? { content: env, name: envName } : null;
  });
  const currentVariables = createMemo(() => {
    const env = currentEnvironment();
    if (env === null) {
      return [];
    }
    return env.content.variables;
  });
  const [showCreateDialog, setShowCreateDialog] = createSignal(false);
  const [showEditDialog, setShowEditDialog] = createSignal(false);

  return (
    <Page
      navigationActions={
        <Row gap="lg">
          <Show when={Object.keys(environments().environments).length > 0}>
            <Row gap="md">
              <label class="text-sm font-light uppercase" for="environment">
                Env
              </label>
              <Select
                id="environment"
                value={shownEnvName() || undefined}
                onChange={setShownEnvName}
                options={environmentNames().map((e) => ({ key: e, label: e }))}
                emptyLabel="-"
              />
            </Row>
          </Show>
          <Button.Root
            class={`${inputBaseStyle} ${inputPrimary}`}
            onClick={() => setShowCreateDialog(true)}
          >
            <Icon path={plus} />
            New
          </Button.Root>
        </Row>
      }
    >
      <Dialog
        title="New Environment"
        showDialog={showCreateDialog()}
        onCloseDialog={() => setShowCreateDialog(false)}
      >
        <CreateEnvForm
          onCreate={({ name, description }) => {
            addEnvironment(name, description);
            setShowCreateDialog(false);
            setShownEnvName(name);
          }}
        />
      </Dialog>
      <Show when={currentEnvironment()} keyed>
        {(env) => (
          <Dialog
            title={`Edit ${env.name}`}
            showDialog={showEditDialog()}
            onCloseDialog={() => setShowEditDialog(false)}
          >
            <Tabs
              tabs={[
                {
                  title: "General",
                  key: "general",
                  element: () => (
                    <EditEnvForm
                      name={env.name}
                      description={env.content.description || ""}
                      onUpdate={({ name, description }) => {
                        updateEnvironment(name, env.name, description);
                        setShownEnvName(name);
                        setShowEditDialog(false);
                      }}
                    />
                  ),
                },
                {
                  key: "advanced",
                  title: "Advanced",
                  element: () => (
                    <Column gap="lg">
                      <div class="rounded-lg px-4 py-3 bg-gray-100">
                        <Column gap="lg">
                          <Heading level="4">Actions</Heading>
                          <div class="max-w-lg">
                            This action will delete the environment and all its
                            variables. Are you absolutely sure?
                          </div>
                          <Button.Root
                            onClick={() => {
                              setShownEnvName(null);
                              deleteEnvironment(env.name);
                              setShowEditDialog(false);
                            }}
                            class={`${inputBaseStyle} ${buttonDanger}`}
                          >
                            <Icon path={trash} /> Delete
                          </Button.Root>
                        </Column>
                      </div>
                    </Column>
                  ),
                },
              ]}
            />
          </Dialog>
        )}
      </Show>
      <Column gap="lg">
        <Show when={currentEnvironment()} keyed>
          {(env) => (
            <div class="flex justify-between items-center">
              <div class="tracking-tight text-sm text-gray-600">
                {env.content.description}
              </div>
              <Button.Root
                class={`${inputBaseStyle} ${inputGray}`}
                onClick={() => setShowEditDialog(true)}
              >
                <Icon path={cog} /> Edit
              </Button.Root>
            </div>
          )}
        </Show>
        <Card>
          <Show when={Object.keys(environments().environments).length === 0}>
            <Information>No Environments created yet.</Information>
          </Show>
          <Show
            when={
              Object.keys(environments().environments).length > 0 &&
              currentEnvironment() === null
            }
          >
            <Information>Select an Environment first.</Information>
          </Show>
          <Show when={currentEnvironment()} keyed>
            {(env) => (
              <Column gap="md">
                <Table
                  headers={["Name", "Value"]}
                  values={currentVariables().map((v, i) => {
                    const isVisible = visibleVariables()[v.key] || false;
                    const handleToggleVisibility = () => {
                      setVisibleVariables({
                        ...visibleVariables(),
                        [v.key]: !isVisible,
                      });
                    };
                    return {
                      data: v,
                      cells: [
                        {
                          value: (
                            <TextField.Root>
                              <TextField.Input
                                class={`${inputBaseStyle} ${inputGray} w-full`}
                                onChange={(ev) => {
                                  const newName = ev.currentTarget.value;
                                  if (!newName) {
                                    return;
                                  }
                                  renameVariable(i, env.name, newName);
                                }}
                                value={v.key}
                                placeholder="Key"
                                autocomplete="off"
                                autocapitalize="off"
                                spellcheck={false}
                              />
                            </TextField.Root>
                          ),
                          wordBreak: "never",
                        },
                        {
                          value: (
                            <div class="flex gap-1 items-stretch">
                              <TextField.Root class="flex-grow">
                                <TextField.Input
                                  type={
                                    v.isSecret && !isVisible
                                      ? "password"
                                      : "text"
                                  }
                                  class={`${inputBaseStyle} ${inputGray} w-full`}
                                  onChange={(ev) =>
                                    updateVariable(
                                      i,
                                      env.name,
                                      v.key,
                                      ev.currentTarget.value
                                    )
                                  }
                                  value={v.value}
                                  placeholder="Value"
                                  autocomplete="off"
                                  autocapitalize="off"
                                  spellcheck={false}
                                />
                              </TextField.Root>
                              <Show when={v.isSecret}>
                                <Button.Root
                                  class={`${inputBaseStyle} ${inputGray}`}
                                  onClick={handleToggleVisibility}
                                >
                                  <Icon path={isVisible ? eyeSlash : eye} />
                                </Button.Root>
                              </Show>
                            </div>
                          ),
                          wordBreak: "always",
                        },
                      ],
                    };
                  })}
                  actions={[
                    (data) => (
                      <Row gap="md">
                        <Button.Root
                          class={`${inputBaseStyle} ${inputGray}`}
                          onClick={() =>
                            toggleVariableVisibility(env.name, data.key)
                          }
                        >
                          <Icon path={data.isSecret ? lockClosed : lockOpen} />
                        </Button.Root>
                        <Button.Root
                          class={`${inputBaseStyle} ${inputGray}`}
                          onClick={() => deleteVariable(env.name, data.key)}
                        >
                          <Icon path={xMark} />
                        </Button.Root>
                      </Row>
                    ),
                  ]}
                />
                <Button.Root
                  class={`${inputBaseStyle} ${inputGray}`}
                  onClick={() => createNewVariable(env.name, "", "")}
                >
                  <Icon path={plus} />
                  Add Variable
                </Button.Root>
              </Column>
            )}
          </Show>
        </Card>
      </Column>
    </Page>
  );
};

export default Environments;
