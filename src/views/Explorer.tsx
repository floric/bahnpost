import { Column } from "../components/Layout";
import { Page } from "../components/Page";
import { Requests } from "./explorer/Requests";
import { Responses } from "./explorer/Responses";
import { ActiveEnv } from "./explorer/ActiveEnv";
import { Tabs } from "../components/Tabs";

const Explorer = () => {
  return (
    <Page navigationActions={<ActiveEnv />}>
      <Column gap="lg">
        <Tabs
          isHeading
          tabs={[
            {
              key: "request",
              title: "New Request",
              element: (setSelectedTab) => (
                <Requests setSelectedTab={setSelectedTab} />
              ),
            },
            {
              key: "response",
              title: "Responses",
              element: () => <Responses />,
            },
          ]}
        />
      </Column>
    </Page>
  );
};

export default Explorer;
