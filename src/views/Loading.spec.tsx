import { describe, expect, test } from "vitest";
import { render } from "@solidjs/testing-library";
import Loading from "./Loading";
import { Route, Router } from "@solidjs/router";

describe("<Loading />", () => {
  test("should render without error", () => {
    const { container } = render(() => (
      <Router>
        <Route path="" component={Loading} />
      </Router>
    ));

    expect(container.innerHTML).toBeTruthy();
  });
});
