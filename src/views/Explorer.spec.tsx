import { afterEach, describe, expect, test } from "vitest";
import { render } from "@solidjs/testing-library";
import Explorer from "./Explorer";
import { Route, Router } from "@solidjs/router";
import { clearMocks, mockIPC } from "@tauri-apps/api/mocks";
import type { ParseValueResult } from "../ipc/bindings.generated";

describe("<Explorer />", () => {
  afterEach(() => {
    clearMocks();
  });

  test("should render without error", () => {
    mockIPC((cmd) => {
      if (cmd === "parse_value") {
        return {
          parts: [
            {
              component: { type: "literal" },
              value: "https://google.de?abc=",
              start: 0,
              end: 10,
            },
            {
              component: {
                type: "variable",
                identifier: "ABC",
                resolvedValue: null,
                env: null,
              },
              value: "{{ABC}}",
              start: 10,
              end: 20,
            },
          ],
        } satisfies ParseValueResult;
      }
    });

    const { container } = render(() => (
      <Router>
        <Route path="" component={Explorer} />
      </Router>
    ));

    expect(container.innerHTML).toBeTruthy();
  });
});
