import { Button, TextField } from "@kobalte/core";
import {
  type FormErrors,
  createForm,
  insert,
  remove,
} from "@modular-forms/solid";
import { type Component, For } from "solid-js";
import { Column } from "../../components/Layout";
import { inputBaseStyle, inputGray, inputPrimary } from "../../utils/styling";
import { FormError, FormInput } from "../../components/Form";
import { Icon, plus, xMark } from "../../components/Icon";
import { Heading } from "../../components/Heading";
import { isValidPublicKey } from "../../ipc/bindings.generated";

type FormContent = { recipients: Array<string> };

export const UpdateRecipientsForm: Component<{
  recipients: Array<string>;
  onUpdate: (form: FormContent) => void;
}> = (props) => {
  const [form, { Form, Field, FieldArray }] = createForm<FormContent>({
    initialValues: { recipients: props.recipients },
    validate: async (form) => {
      const errors: FormErrors<FormContent> = {};
      const validationResults = await Promise.all(
        form.recipients?.map(async (r, i) => ({
          i,
          isValid: await isValidPublicKey(r),
        })) || []
      );
      validationResults
        .filter(({ isValid }) => !isValid)
        .forEach(({ i }) => {
          errors[`recipients.${i}`] = "Invalid Key Format";
        });
      return errors;
    },
  });
  return (
    <Form onSubmit={props.onUpdate}>
      <Column gap="lg">
        <Heading level="4">Recipients</Heading>
        <Column gap="md">
          <FieldArray name="recipients">
            {(fieldArray) => (
              <For each={fieldArray.items}>
                {(_, index) => (
                  <Field name={`recipients.${index()}`}>
                    {(field, props) => (
                      <TextField.Root
                        validationState={field.error ? "invalid" : "valid"}
                      >
                        <div class="flex gap-2 items-stretch">
                          <FormInput field={field} {...props} />
                          <Button.Root
                            class={`${inputBaseStyle} ${inputGray}`}
                            onClick={() =>
                              remove(form, "recipients", { at: index() })
                            }
                          >
                            <Icon path={xMark} />
                          </Button.Root>
                        </div>
                        <FormError field={field} />
                      </TextField.Root>
                    )}
                  </Field>
                )}
              </For>
            )}
          </FieldArray>
          <Button.Root
            class={`${inputBaseStyle} ${inputGray}`}
            onClick={() => insert(form, "recipients", { value: "" })}
          >
            <Icon path={plus} />
            Add Recipient
          </Button.Root>
        </Column>
        <Button.Root class={`${inputBaseStyle} ${inputPrimary}`} type="submit">
          Update Recipients
        </Button.Root>
      </Column>
    </Form>
  );
};
