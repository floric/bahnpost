# myzel

A simple and privacy friendly REST API client.

## Goals

- lightweight and fast
- full featured for HTTP requests
- privacy and security focused (no cloud syncs, age encrypted secrets)
- developer friendly (config as code)
- easy to use and accessible

## Thanks

This project is heavily based on some great projects by awesome people, just to name a few:

- [Tauri](https://tauri.app)
- [SolidJS](https://www.solidjs.com)
- [Kobalte](https://kobalte.dev/docs/core/overview/introduction)
- [Tailwind CSS](https://tailwindcss.com) & [heroicons](https://heroicons.com/)
- [Vite](https://vitejs.dev)
- [age](https://github.com/str4d/rage)

Thanks for all your hard work.

## Get Started

### Start Development

You will need the following tools:

- [Tauri and its prerequisites including Rust](https://tauri.app/v1/guides/getting-started/prerequisites)
- [Node.js LTS](https://nodejs.org/en/)
- [PnPm](https://pnpm.io/)

I recommend using [VS Code](https://code.visualstudio.com/) for development, as this project also contains a basic configuration and recommended extensions for a fast and easy development start.

After installing the tools above, you should be able to start the development server:

```
pnpm i --ignore-scripts
RUST_LOG=debug pnpm tauri dev -- --features generate-types
```

Alternatively you can simply use the launch config "Run Dev" provided for VS Code.

### Build Releases

Releases are built with the Tauri CLI:

```
pnpm tauri build
```

### Tests

#### Frontend

The frontend tests are based on [Vite](https://vitejs.dev/) for running the unit tests and [expect](https://jestjs.io/docs/expect) for assertions.

```
pnpm run test
```

#### Backend

The backend tests use [Insta](https://insta.rs/) for Snapshot tests.

```
cd src-tauri
cargo test
```

To update snapshots, run the following:

```
cargo insta review
```

### Linting

#### Frontend

Linting is done with [ESLint](https://eslint.org/) and extended with the [Solid ESLint Plugin](https://github.com/solidjs-community/eslint-plugin-solid).

```
pnpm run lint
```

Backend:

```
cd src-tauri
cargo clippy
```
