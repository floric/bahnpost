#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]
use crate::{
    commands::{
        requests::{resolve_request, send_request},
        variables::parse_value,
        workspaces::{is_valid_public_key, load_workspace, update_workspace},
    },
    state::active_workspace::ActiveWorkspace,
};
use log::info;
use tauri::{AboutMetadata, Menu, MenuItem, Submenu, WindowMenuEvent};

mod commands;
mod config;
mod model;
mod shared;
mod state;

fn main() {
    env_logger::init();

    info!("starting up");

    update_typescript_types();

    let menu = build_menu();
    tauri::Builder::default()
        .manage(ActiveWorkspace::default())
        .menu(menu)
        .on_menu_event(on_menu_event)
        .invoke_handler(tauri::generate_handler![
            // requests
            send_request,
            resolve_request,
            // workspaces
            load_workspace,
            update_workspace,
            is_valid_public_key,
            // variables
            parse_value,
            // encryption
        ])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}

fn build_menu() -> Menu {
    Menu::new()
        .add_submenu(Submenu::new(
            "myzel",
            Menu::new()
                .add_native_item(MenuItem::About(
                    "myzel".to_string(),
                    AboutMetadata::default(),
                ))
                .add_native_item(MenuItem::Separator)
                .add_native_item(MenuItem::Quit),
        ))
        .add_submenu(Submenu::new(
            "Edit",
            Menu::new()
                .add_native_item(MenuItem::SelectAll)
                .add_native_item(MenuItem::Cut)
                .add_native_item(MenuItem::Copy)
                .add_native_item(MenuItem::Paste),
        ))
}

fn on_menu_event(event: WindowMenuEvent) {
    if event.menu_item_id() == "quit" {
        std::process::exit(0);
    }
}

#[cfg(feature = "generate-types")]
fn update_typescript_types() {
    use std::path::Path;

    let types_path = "../src/ipc/bindings.generated.ts";
    let absolute_path = Path::new(types_path).to_str().unwrap();

    tauri_specta::ts::export(
        specta::collect_types![
            update_workspace,
            load_workspace,
            parse_value,
            send_request,
            resolve_request,
            is_valid_public_key
        ],
        types_path,
    )
    .unwrap();

    info!("updated typescript types at {}", absolute_path);
}

#[cfg(not(feature = "generate-types"))]
fn update_typescript_types() {
    info!("Skipped updating typescript types");
}
