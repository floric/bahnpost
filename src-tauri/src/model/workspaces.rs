use serde::{Deserialize, Serialize};
use std::collections::BTreeSet;

#[cfg_attr(feature = "generate-types", derive(specta::Type))]
#[derive(Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Variable {
    pub key: String,
    pub value: String,
    pub is_secret: bool,
}

#[cfg_attr(feature = "generate-types", derive(specta::Type))]
#[derive(Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Environment {
    pub name: String,
    pub description: Option<String>,
    pub variables: Vec<Variable>,
}

impl Environment {
    pub fn get_variable(&self, key: &str) -> Option<Variable> {
        self.variables.iter().find(|v| v.key.eq(key)).cloned()
    }

    pub fn variable_keys(&self) -> BTreeSet<String> {
        self.variables.iter().map(|v| v.key.clone()).collect()
    }
}

// TODO add request collections and maybe also tests
#[cfg_attr(feature = "generate-types", derive(specta::Type))]
#[derive(Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Workspace {
    pub name: String,
    pub environments: Vec<Environment>,
    pub description: Option<String>,
    pub file_name: String,
    pub recipients: Vec<String>,
}

#[cfg(test)]
mod tests {
    use crate::model::workspaces::{Environment, Variable, Workspace};

    #[test]
    fn should_serialize_workspace() {
        // given
        let workspace = Workspace {
            name: "name".to_string(),
            file_name: "fileName.json".to_string(),
            description: Some("description".to_string()),
            environments: vec![
                Environment {
                    name: "DEV".to_string(),
                    description: Some("dev env".to_string()),
                    variables: Vec::new(),
                },
                Environment {
                    name: "PROD".to_string(),
                    description: None,
                    variables: vec![
                        Variable {
                            key: "SECRET".to_string(),
                            value: "some 123".to_string(),
                            is_secret: true,
                        },
                        Variable {
                            key: "PUBLIC".to_string(),
                            value: "hey there".to_string(),
                            is_secret: false,
                        },
                    ],
                },
            ],
            recipients: vec!["rec-a".to_string(), "rec-b".to_string()],
        };

        // when
        let serialized = serde_json::to_string(&workspace).expect("serialization failed");

        // then
        insta::assert_json_snapshot!(&serialized);
    }
}
