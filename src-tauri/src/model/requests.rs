use serde::{Deserialize, Serialize};
use time::OffsetDateTime;

#[cfg_attr(feature = "generate-types", derive(specta::Type))]
#[derive(Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct HeaderEntry {
    pub name: String,
    pub value: String,
}

#[cfg_attr(feature = "generate-types", derive(specta::Type))]
#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RequestData {
    pub uri: String,
    pub method: String,
    pub headers: Vec<HeaderEntry>,
    pub body: Option<String>,
    pub active_env_name: Option<String>,
}

#[cfg_attr(feature = "generate-types", derive(specta::Type))]
#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ResolvedRequestData {
    pub uri: String,
    pub headers: Vec<HeaderEntry>,
}

#[cfg_attr(feature = "generate-types", derive(specta::Type))]
#[derive(Serialize)]
#[serde(rename_all = "camelCase", tag = "type")]
pub enum SendRequestResponse {
    #[serde(rename_all = "camelCase")]
    Error {
        message: String,
        details: Option<String>,
    },
    #[serde(rename_all = "camelCase")]
    Success {
        status: u16,
        status_text: String,
        headers: Vec<HeaderEntry>,
        body: String,
        body_size: u32,
        duration_ms: u32,
    },
}

#[cfg_attr(feature = "generate-types", derive(specta::Type))]
#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct SendRequestResult {
    pub response: SendRequestResponse,
    pub request_data: RequestData,
    pub resolved_data: ResolvedRequestData,
    #[serde(with = "time::serde::rfc3339")]
    pub date_time: OffsetDateTime,
}

#[cfg(test)]
mod tests {
    use crate::model::requests::{
        HeaderEntry, RequestData, ResolvedRequestData, SendRequestResponse, SendRequestResult,
    };
    use time::macros::datetime;

    #[test]
    fn should_serialize_send_request_result_with_success() {
        // given
        let result = SendRequestResult {
            response: SendRequestResponse::Success {
                status: 200,
                status_text: "OK".to_string(),
                body: "some".to_string(),
                body_size: 123,
                duration_ms: 9876,
                headers: vec![HeaderEntry {
                    name: "Cache-Control".to_string(),
                    value: "no-cache".to_string(),
                }],
            },
            request_data: RequestData {
                uri: "https://some.de".to_string(),
                method: "GET".to_string(),
                active_env_name: Some("DEV".to_string()),
                body: None,
                headers: vec![HeaderEntry {
                    name: "Authorization".to_string(),
                    value: "Bearer abcdef".to_string(),
                }],
            },
            resolved_data: ResolvedRequestData {
                uri: "https://some.de".to_string(),
                headers: Vec::new(),
            },
            date_time: datetime!(2023-01-07 11:23 UTC),
        };

        // when
        let serialized = serde_json::to_string(&result).expect("serialization failed");

        // then
        insta::assert_json_snapshot!(&serialized);
    }

    #[test]
    fn should_serialize_send_request_result_with_error() {
        // given
        let result = SendRequestResult {
            response: SendRequestResponse::Error {
                message: "message".to_string(),
                details: Some("details".to_string()),
            },
            request_data: RequestData {
                uri: "https://some.de".to_string(),
                method: "GET".to_string(),
                active_env_name: None,
                body: None,
                headers: Vec::new(),
            },
            resolved_data: ResolvedRequestData {
                uri: "https://some.de".to_string(),
                headers: Vec::new(),
            },
            date_time: datetime!(2023-01-07 11:23 UTC),
        };

        // when
        let serialized = serde_json::to_string(&result).expect("serialization failed");

        // then
        insta::assert_json_snapshot!(&serialized);
    }
}
