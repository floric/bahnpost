use serde::Serialize;

#[cfg_attr(feature = "generate-types", derive(specta::Type))]
#[derive(Serialize)]
#[serde(rename_all = "camelCase", tag = "mode")]
pub enum ComputedValue {
    UuidV4 { generated: String },
    RandomAlphanumeric { generated: String },
}

#[cfg_attr(feature = "generate-types", derive(specta::Type))]
#[derive(Serialize)]
#[serde(rename_all = "camelCase", tag = "type")]
pub enum ValueComponent {
    #[serde(rename_all = "camelCase")]
    Variable {
        identifier: String,
        resolved_value: Option<String>,
        env: Option<String>,
    },
    Computed(ComputedValue),
    Literal,
}

#[cfg_attr(feature = "generate-types", derive(specta::Type))]
#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ValuePart {
    pub value: String,
    pub start: u32,
    pub end: u32,
    pub component: ValueComponent,
}

#[cfg_attr(feature = "generate-types", derive(specta::Type))]
#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ParseValueResult {
    pub parts: Vec<ValuePart>,
}

#[cfg(test)]
mod tests {
    use crate::model::variables::{ComputedValue, ParseValueResult, ValueComponent, ValuePart};

    #[test]
    fn should_serialize_parse_value_result() {
        // given
        let result = ParseValueResult {
            parts: vec![
                ValuePart {
                    value: "abc".to_string(),
                    start: 0,
                    end: 10,
                    component: ValueComponent::Literal,
                },
                ValuePart {
                    value: "{{ABC_TEST}}".to_string(),
                    start: 10,
                    end: 20,
                    component: ValueComponent::Variable {
                        identifier: "ABC".to_string(),
                        resolved_value: Some("myVal".to_string()),
                        env: Some("DEV".to_string()),
                    },
                },
                ValuePart {
                    value: "{{ABC_TEST}}".to_string(),
                    start: 10,
                    end: 20,
                    component: ValueComponent::Computed(ComputedValue::UuidV4 {
                        generated: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000".to_string(),
                    }),
                },
            ],
        };

        // when
        let serialized = serde_json::to_string(&result).expect("serialization failed");

        // then
        insta::assert_json_snapshot!(&serialized);
    }
}
