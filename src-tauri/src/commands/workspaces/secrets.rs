use age::x25519::Recipient;
use base64::{engine::general_purpose, Engine};
use std::io::{Read, Write};
use std::str::FromStr;

/// Encrypts a secret using age encoded in base64.
pub fn encrypt_secret(secret: &str, recipients: &[&str]) -> Result<String, String> {
    let parsed_recipients = recipients
        .iter()
        .map(|r| create_boxed_recipient(r))
        .collect::<Result<Vec<_>, _>>()?;
    let encryptor =
        age::Encryptor::with_recipients(parsed_recipients).expect("initializing encryptor failed");

    let mut encrypted = vec![];
    let mut writer = encryptor
        .wrap_output(&mut encrypted)
        .map_err(|err| format!("writing secret failed: {err}"))?;
    writer
        .write_all(secret.as_bytes())
        .map_err(|err| format!("writing secret failed: {err}"))?;
    writer
        .finish()
        .map_err(|err| format!("writing secret failed: {err}"))?;

    Ok(general_purpose::STANDARD.encode(&encrypted))
}

/// Decrypts an base64 encoded secret encrypted with age.
pub fn decrypt_secret(secret: &str, secret_key: &str) -> Result<String, String> {
    let decoded = general_purpose::STANDARD
        .decode(secret)
        .expect("decoding from base64 failed");

    let decryptor = match age::Decryptor::new(&decoded[..]).expect("initializing decryptor failed")
    {
        age::Decryptor::Recipients(d) => d,
        _ => unreachable!(),
    };

    let mut decrypted = vec![];
    let mut reader = decryptor
        .decrypt(std::iter::once(
            &age::x25519::Identity::from_str(secret_key)
                .map_err(|err| format!("parsing secret key failed: {}", err))?
                as &dyn age::Identity,
        ))
        .map_err(|err| format!("decryption failed: {}", err))?;
    reader
        .read_to_end(&mut decrypted)
        .expect("reading decrypted value failed");

    Ok(std::str::from_utf8(&decrypted)
        .expect("converting secret to utf8 failed")
        .to_string())
}

fn create_boxed_recipient(id: &str) -> Result<Box<dyn age::Recipient + Send + 'static>, String> {
    let recipient =
        Recipient::from_str(id).map_err(|err| format!("parsing recipient failed: {err}"))?;

    Ok(Box::new(recipient))
}

#[cfg(test)]
mod tests {
    use super::encrypt_secret;
    use crate::commands::workspaces::secrets::decrypt_secret;

    const PUBLIC_TEST_KEY: &str = "age1te0779huf8ryye0hk57j57j37xqws4mn0vefr9plruf0286mlflsfwsc53";
    const PRIVATE_TEST_KEY: &str =
        "AGE-SECRET-KEY-120FC9QYG2TATRSAAH7CQWAUQS0KTRWSCTFZFA2V3DSYXJ4EN5HTQUGDDXE";

    #[test]
    fn should_encrypt_secret() {
        // given
        let secret = "my-secret-123";

        // when
        let encrypted = encrypt_secret(
            secret,
            &[
                PUBLIC_TEST_KEY,
                "age1mn5tlex0zul0467lm9nq7yyhgq7xfxa0eg4gdg6r20ctfcej6stqwhqlna",
            ],
        )
        .expect("encryption failed");

        // then
        assert!(encrypted.starts_with("YWdlLWVuY3J5cHRpb24ub3JnL3YxCi0"));
    }

    #[test]
    fn should_not_encrypt_secret_with_invalid_public_key() {
        // given
        let secret = "my-secret-123";

        // when
        let result = encrypt_secret(secret, &["abc"]);

        // then
        assert_eq!(
            "parsing recipient failed: invalid Bech32 encoding",
            result.unwrap_err()
        );
    }

    #[test]
    fn should_decrypt_secret() {
        // given
        let encrypted = "YWdlLWVuY3J5cHRpb24ub3JnL3YxCi0+IFgyNTUxOSBtVGczYUtkZ2gxRHR5QjdIMjBwbW1mcDZ5YlF1SnFFUStkMDNSa1NpMENFClllU1RwTWFYb2M3V0ljSmJobFM1Yk10TXBkaXhUZHcxd1grV0J6ZmMwaGcKLT4gPiNWX2QtZ3JlYXNlIFJTQVhIP0wgVkNBZE1hID4qTgpnSGtZdmtPNWJRMmkzeWlML0M2T3NxMk1leDVtM2lteFRvVktjMWttcy92ZXhocVBxNWZVTDNVZmNUMFhPRWxDCmRYbHdaR0ZsQkJrbAotLS0gZjlYOFJqdDczU05Va3RqTXZPSDVneVdMSnpGSHpydWF1N1N0bUNBU3VtcwosnAgcW5SXHVtgQWUZCz/xXq0tsu3MqpzjHNLpezPYOD6q1CtN1eKyqKxho3s=";

        // when
        let decrypted = decrypt_secret(encrypted, PRIVATE_TEST_KEY).expect("decryption failed");

        // then
        assert_eq!("my-secret-123", &decrypted);
    }

    #[test]
    fn should_not_decrypt_secret_with_wrong_secret_key() {
        // given
        let encrypted = "YWdlLWVuY3J5cHRpb24ub3JnL3YxCi0+IFgyNTUxOSBtVGczYUtkZ2gxRHR5QjdIMjBwbW1mcDZ5YlF1SnFFUStkMDNSa1NpMENFClllU1RwTWFYb2M3V0ljSmJobFM1Yk10TXBkaXhUZHcxd1grV0J6ZmMwaGcKLT4gPiNWX2QtZ3JlYXNlIFJTQVhIP0wgVkNBZE1hID4qTgpnSGtZdmtPNWJRMmkzeWlML0M2T3NxMk1leDVtM2lteFRvVktjMWttcy92ZXhocVBxNWZVTDNVZmNUMFhPRWxDCmRYbHdaR0ZsQkJrbAotLS0gZjlYOFJqdDczU05Va3RqTXZPSDVneVdMSnpGSHpydWF1N1N0bUNBU3VtcwosnAgcW5SXHVtgQWUZCz/xXq0tsu3MqpzjHNLpezPYOD6q1CtN1eKyqKxho3s=";
        let different_key =
            "AGE-SECRET-KEY-1DLKEWY8WYTGL537KHWU78WM6HT7XJ9WMR39VMQ2NFZWY8S7RLM5QFUAEJP";

        // when
        let result = decrypt_secret(encrypted, different_key);

        // then
        assert_eq!(
            "decryption failed: No matching keys found",
            result.unwrap_err()
        );
    }

    #[test]
    fn should_not_decrypt_secret_with_broken_secret_key() {
        // given
        let encrypted = "YWdlLWVuY3J5cHRpb24ub3JnL3YxCi0+IFgyNTUxOSBtVGczYUtkZ2gxRHR5QjdIMjBwbW1mcDZ5YlF1SnFFUStkMDNSa1NpMENFClllU1RwTWFYb2M3V0ljSmJobFM1Yk10TXBkaXhUZHcxd1grV0J6ZmMwaGcKLT4gPiNWX2QtZ3JlYXNlIFJTQVhIP0wgVkNBZE1hID4qTgpnSGtZdmtPNWJRMmkzeWlML0M2T3NxMk1leDVtM2lteFRvVktjMWttcy92ZXhocVBxNWZVTDNVZmNUMFhPRWxDCmRYbHdaR0ZsQkJrbAotLS0gZjlYOFJqdDczU05Va3RqTXZPSDVneVdMSnpGSHpydWF1N1N0bUNBU3VtcwosnAgcW5SXHVtgQWUZCz/xXq0tsu3MqpzjHNLpezPYOD6q1CtN1eKyqKxho3s=";
        let different_key = "AGE-SECRET-KEY-123";

        // when
        let result = decrypt_secret(encrypted, different_key);

        // then
        assert_eq!(
            "parsing secret key failed: invalid Bech32 encoding",
            result.unwrap_err()
        );
    }

    #[test]
    fn should_encrypt_and_decrypt_complex_secret() {
        // given
        let secret = "abcABC123\"'*!$%/§()=?_-.,+#´`<>^°äöüß😱@
        multi
        line";

        // when
        let encrypted = encrypt_secret(secret, &[PUBLIC_TEST_KEY]).expect("encryption failed");
        let decrypted = decrypt_secret(&encrypted, PRIVATE_TEST_KEY).expect("decryption failed");

        // then
        assert_eq!(&secret, &decrypted);
    }
}
