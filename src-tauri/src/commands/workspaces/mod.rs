use self::secrets::decrypt_secret;
use crate::{
    commands::workspaces::secrets::encrypt_secret,
    model::workspaces::{Environment, Variable, Workspace},
    state::active_workspace::ActiveWorkspace,
};
use age::x25519::Recipient;
use log::{debug, warn};
use std::{
    fs::File,
    path::{Path, PathBuf},
    str::FromStr,
};
use tauri::State;

// TODO use private key from user settings instead
const PRIVATE_TEST_KEY: &str =
    "AGE-SECRET-KEY-120FC9QYG2TATRSAAH7CQWAUQS0KTRWSCTFZFA2V3DSYXJ4EN5HTQUGDDXE";

mod secrets;

/// Loads a mocked workspace (which is a Git compatible folder with environments and request collections).
/// In the future, this method should be able to load a specific workspace based on a name or path.
#[tauri::command]
#[cfg_attr(feature = "generate-types", specta::specta)]
pub fn load_workspace(
    window: tauri::Window,
    file_name: String,
    active_workspace: State<'_, ActiveWorkspace>,
) -> Result<Workspace, String> {
    debug!("loading workspace at {}", file_name);

    // only for testing
    let test_file_name = "dev-ws.json";
    if file_name.eq(test_file_name) && File::open(get_file_in_main_dir(&file_name)?).is_err() {
        warn!("writing test workspace for development");
        let dev_ws = Workspace {
            name: "DEV".to_string(),
            description: Some("Test Workspace for Development".to_string()),
            file_name: test_file_name.to_string(),
            environments: vec![Environment {
                name: "TST".to_string(),
                description: Some("For testing only".to_string()),
                variables: vec![
                    Variable {
                        key: "ABC".to_string(),
                        value: "abc".to_string(),
                        is_secret: true,
                    },
                    Variable {
                        key: "XYZ".to_string(),
                        value: "xyz".to_string(),
                        is_secret: false,
                    },
                ],
            }],
            recipients: Vec::new(),
        };
        write_workspace_file(&dev_ws)?;
        active_workspace.update(&dev_ws);
    }

    let workspace = read_workspace_file(&file_name)?;

    window
        .set_title(&format!("myzel - {}", workspace.name))
        .expect("setting title failed");

    Ok(workspace)
}

/// Updates a workspace and later persists it to the workspace folder.
#[tauri::command]
#[cfg_attr(feature = "generate-types", specta::specta)]
pub async fn update_workspace(
    workspace: Workspace,
    active_workspace: State<'_, ActiveWorkspace>,
) -> Result<(), String> {
    debug!(
        "updating workspace {} at {}",
        &workspace.name, &workspace.file_name
    );

    write_workspace_file(&workspace)?;
    active_workspace.update(&workspace);

    Ok(())
}

/// Returns true if the public key has a valid format.
#[tauri::command]
#[cfg_attr(feature = "generate-types", specta::specta)]
pub fn is_valid_public_key(key: String) -> bool {
    let is_valid_key = Recipient::from_str(&key).map(|_| true).unwrap_or(false);

    if !is_valid_key {
        warn!("validated malformed public key: {}", &key);
    }

    is_valid_key
}

fn write_workspace_file(workspace: &Workspace) -> Result<(), String> {
    let myzel_dir = get_file_in_main_dir(&workspace.file_name)?;
    let recipients = workspace
        .recipients
        .iter()
        .map(|r| r.as_ref())
        .collect::<Vec<_>>();

    let mut encrypted_workspace = workspace.clone();
    for e in encrypted_workspace.environments.iter_mut() {
        for v in e.variables.iter_mut().filter(|v| v.is_secret) {
            v.value = encrypt_secret(&v.value, &recipients)?;
        }
    }

    let file_content = serde_json::to_string_pretty(&encrypted_workspace)
        .map_err(|err| format!("Serializing workspace failed: {:?}", err))?;

    std::fs::write(myzel_dir, file_content)
        .map_err(|err| format!("Writing workspace failed: {:?}", err))?;

    debug!(
        "wrote encrypted workspace file {} successfully",
        &workspace.file_name
    );

    Ok(())
}

fn read_workspace_file(file_name: &str) -> Result<Workspace, String> {
    let myzel_dir = get_file_in_main_dir(file_name)?;

    let text = std::fs::read_to_string(myzel_dir)
        .map_err(|err| format!("Reading workspace failed: {:?}", err))?;

    let encrypted_workspace = serde_json::from_str::<Workspace>(&text)
        .map_err(|err| format!("Parsing workspace failed: {:?}", err))?;

    let mut decrypted_workspace = encrypted_workspace;
    for e in decrypted_workspace.environments.iter_mut() {
        for v in e.variables.iter_mut().filter(|v| v.is_secret) {
            v.value = decrypt_secret(&v.value, PRIVATE_TEST_KEY)?;
        }
    }

    Ok(decrypted_workspace)
}

fn get_file_in_main_dir(file_name: &str) -> Result<PathBuf, String> {
    let myzel_dir =
        Path::new(&dirs::home_dir().expect("Reading home dir path failed")).join("myzel");
    if !myzel_dir.exists() {
        std::fs::create_dir(&myzel_dir)
            .map_err(|err| format!("Writing myzel directory in home dir failed: {:?}", err))?;
    }

    Ok(Path::new(&myzel_dir).join(file_name))
}
