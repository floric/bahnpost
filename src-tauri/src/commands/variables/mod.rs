use crate::{
    model::variables::ParseValueResult, shared::parser, state::active_workspace::ActiveWorkspace,
};
use tauri::State;

#[tauri::command]
#[cfg_attr(feature = "generate-types", specta::specta)]
pub fn parse_value(
    value: String,
    active_env: Option<String>,
    active_workspace: State<'_, ActiveWorkspace>,
) -> Result<ParseValueResult, String> {
    parser::parse_value(
        &value,
        &active_env.and_then(|e| active_workspace.env_by_name(&e)),
    )
}
