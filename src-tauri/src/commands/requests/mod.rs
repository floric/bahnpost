use crate::{
    model::requests::{RequestData, ResolvedRequestData, SendRequestResult},
    shared::requests,
    state::active_workspace::ActiveWorkspace,
};
use tauri::State;

#[tauri::command]
#[cfg_attr(feature = "generate-types", specta::specta)]
pub async fn send_request(
    data: RequestData,
    active_workspace: State<'_, ActiveWorkspace>,
) -> Result<SendRequestResult, String> {
    requests::send_request(&data, active_workspace).await
}

#[tauri::command]
#[cfg_attr(feature = "generate-types", specta::specta)]
pub async fn resolve_request(
    data: RequestData,
    active_workspace: State<'_, ActiveWorkspace>,
) -> Result<ResolvedRequestData, String> {
    requests::resolve_data(&data, &active_workspace)
}
