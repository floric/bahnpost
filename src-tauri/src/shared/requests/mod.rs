use crate::{
    model::{
        requests::{
            HeaderEntry, RequestData, ResolvedRequestData, SendRequestResponse, SendRequestResult,
        },
        variables::{ComputedValue, ValueComponent},
        workspaces::Environment,
    },
    shared::parser,
    state::active_workspace::ActiveWorkspace,
};
use log::debug;
use std::{
    mem::size_of_val,
    str::FromStr,
    time::{Duration, Instant},
};
use tauri::{
    api::{
        http::{Client, ClientBuilder, HttpRequestBuilder, Response, ResponseType},
        Error,
    },
    http::header::{HeaderMap, HeaderName},
    State,
};
use time::OffsetDateTime;

fn build_http_client() -> Client {
    // disable redirects for now, but support redirections as option in the future
    ClientBuilder::new()
        .max_redirections(0)
        .build()
        .expect("Building REST client failed")
}

pub async fn send_request(
    data: &RequestData,
    active_workspace: State<'_, ActiveWorkspace>,
) -> Result<SendRequestResult, String> {
    debug!(
        "received incoming request to {} {} with name {}",
        &data.method,
        &data.uri,
        &data
            .active_env_name
            .clone()
            .unwrap_or_else(|| "-".to_string()),
    );

    let resolved_data = resolve_data(data, &active_workspace)?;

    let request_data = RequestData {
        uri: data.uri.clone(),
        method: data.method.clone(),
        headers: data.headers.clone(),
        body: data.body.clone(),
        active_env_name: data.active_env_name.clone(),
    };
    let request = build_request(
        &data.uri,
        &data.method,
        data.headers.clone(),
        data.body.clone(),
    )
    .await?;
    let client = build_http_client();

    // send request with reqwest
    // TODO consider using reqwest directly for more information
    let start = Instant::now();
    let response = client.send(request).await;
    if let Err(error) = response {
        let response = handle_error(error)?;
        return Ok(SendRequestResult {
            response,
            request_data,
            resolved_data,
            date_time: OffsetDateTime::now_utc(),
        });
    }

    let duration = start.elapsed();
    let response = response.unwrap();

    map_response(response, duration, request_data, resolved_data).await
}

fn handle_error(error: Error) -> Result<SendRequestResponse, String> {
    match error {
        tauri::api::Error::Network(network_error) => Ok(SendRequestResponse::Error {
            message: "Sending failed".to_string(),
            details: Some(network_error.to_string()),
        }),
        tauri::api::Error::Io(io_error) => Err(format!("IO Error ({io_error})")),
        tauri::api::Error::Url(url_error) => Err(format!("URL Error ({url_error})")),
        tauri::api::Error::Http(http_error) => Err(format!("Http Error ({http_error})")),
        _ => Err(format!("Unexpected Sending Error ({error})")),
    }
}

fn resolve_uri(uri: &str, environment: &Option<Environment>) -> Result<String, String> {
    let parsed_uri = parser::parse_value(uri, &None)?;

    if let Some(error) = verify_preconditions(environment, &parsed_uri) {
        return Err(error);
    }

    let resolved_uri = parsed_uri
        .parts
        .iter()
        .map(|p| match &p.component {
            ValueComponent::Variable { identifier, .. } => environment
                .as_ref()
                .and_then(|e| e.get_variable(identifier))
                .map(|v| v.value)
                .expect("resolving variable failed"),
            ValueComponent::Computed(computed) => match computed {
                ComputedValue::UuidV4 { generated } => generated.clone(),
                ComputedValue::RandomAlphanumeric { generated } => generated.clone(),
            },
            ValueComponent::Literal => p.value.clone(),
        })
        .reduce(|a, b| format!("{}{}", a, b))
        .unwrap_or_default();

    Ok(resolved_uri)
}

pub fn resolve_data(
    data: &RequestData,
    active_workspace: &State<'_, ActiveWorkspace>,
) -> Result<ResolvedRequestData, String> {
    let environment = data
        .active_env_name
        .as_ref()
        .and_then(|env_name| active_workspace.env_by_name(env_name));

    let resolved_uri: String = resolve_uri(&data.uri, &environment)?;

    let resolved_data = ResolvedRequestData {
        uri: resolved_uri,
        headers: Vec::new(),
    };

    Ok(resolved_data)
}

fn verify_preconditions(
    environment: &Option<Environment>,
    parsed_uri: &crate::model::variables::ParseValueResult,
) -> Option<String> {
    let variable_keys = environment
        .as_ref()
        .map(|e| e.variable_keys())
        .unwrap_or_default();
    debug!("Using environment variables: {:?}", variable_keys);

    for v in parsed_uri.parts.iter().flat_map(|p| match &p.component {
        ValueComponent::Variable { identifier, .. } => Some(identifier),
        _ => None,
    }) {
        if !variable_keys.contains(v) {
            return Some(format!("Variable {} missing in Environment", v));
        }
    }

    None
}

async fn build_request(
    uri: &str,
    method: &str,
    headers: Vec<HeaderEntry>,
    body: Option<String>,
) -> Result<HttpRequestBuilder, String> {
    let mut header_map = HeaderMap::new();
    for header in headers.iter().filter(|h| !h.name.is_empty()) {
        let header = header.to_owned();
        let _ = header_map.insert(
            HeaderName::from_str(&header.name)
                .map_err(|_| format!("Building header failed: {}", header.name))?,
            (header.value.clone().as_str())
                .parse()
                .map_err(|_| format!("Parsing header content failed: {}", header.value))?,
        );
    }
    let mut request = HttpRequestBuilder::new(method, uri)
        .map_err(|err| format!("Building request failed: {err}"))?
        .headers(header_map)
        .response_type(ResponseType::Binary);
    if let Some(body) = body {
        request = request.body(tauri::api::http::Body::Text(body));
    }

    Ok(request)
}

async fn map_response(
    response: Response,
    duration: Duration,
    request_data: RequestData,
    resolved_data: ResolvedRequestData,
) -> Result<SendRequestResult, String> {
    let mut headers = Vec::new();
    for (name, value) in response.headers().iter() {
        headers.push(HeaderEntry {
            name: name.to_string(),
            value: value
                .to_str()
                .map(|val| val.to_string())
                .map_err(|err| format!("Parsing header response failed: {err}"))?,
        });
    }
    // sort by name for easiert reading
    headers.sort_by(|a, b| a.name.cmp(&b.name));

    let status = response.status().as_u16();
    let status_text = response
        .status()
        .canonical_reason()
        .unwrap_or("Unknown")
        .to_string();
    let bytes = response.bytes().await;
    let body_size = bytes
        .as_ref()
        .map(|raw| size_of_val(&*raw.data).try_into().unwrap())
        .map_err(|err| format!("Determining body size failed: {err}"))?;
    let body = bytes
        .map(|raw| String::from_utf8_lossy(&raw.data).to_string())
        .map_err(|err| format!("Parsing response body failed: {err}"))?;

    let response = SendRequestResponse::Success {
        status,
        status_text,
        body,
        body_size,
        headers,
        duration_ms: duration.as_millis().try_into().unwrap(),
    };

    Ok(SendRequestResult {
        response,
        request_data,
        resolved_data,
        date_time: OffsetDateTime::now_utc(),
    })
}
