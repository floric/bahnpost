use crate::model::{
    variables::{ComputedValue, ParseValueResult, ValueComponent, ValuePart},
    workspaces::Environment,
};
use regex::Regex;

pub fn parse_value(
    value: &str,
    active_env: &Option<Environment>,
) -> Result<ParseValueResult, String> {
    let mut variables = determine_variable_matches(value, active_env);
    let mut computed = determine_computed_matches(value);
    let mut all = Vec::new();
    all.append(&mut variables);
    all.append(&mut computed);
    all.sort_by(|a, b| a.start.cmp(&b.start));

    // iterate through parts and concat them with parsed values
    let mut parts = Vec::new();
    let mut i: u32 = 0;
    for v in all {
        if i < v.start {
            let literal_value = value
                .chars()
                .skip((i).try_into().unwrap())
                .take((v.start - i).try_into().unwrap())
                .collect();
            parts.push(ValuePart {
                value: literal_value,
                start: i,
                end: v.start - 1,
                component: ValueComponent::Literal,
            });
        }
        i = v.end;
        parts.push(v);
    }

    // add last part
    let len: u32 = (value.len()).try_into().unwrap();
    if i != len {
        let literal_value = value
            .chars()
            .skip((i).try_into().unwrap())
            .take((len - i).try_into().unwrap())
            .collect();
        parts.push(ValuePart {
            value: literal_value,
            start: i,
            end: len,
            component: ValueComponent::Literal,
        });
    }

    Ok(ParseValueResult { parts })
}

fn determine_variable_matches(value: &str, active_env: &Option<Environment>) -> Vec<ValuePart> {
    let variable_regex = Regex::new(r"(\{\{([[:word:]]|_)+\}\})").unwrap();
    variable_regex
        .find_iter(value)
        .flat_map(|p| {
            let matched_value = p.as_str().to_string();
            let identifier = extract_variable_identifier(&matched_value);
            let matched_variable = active_env
                .as_ref()
                .and_then(|e| e.get_variable(&identifier));
            vec![ValuePart {
                start: p.start() as u32,
                end: p.end() as u32,
                component: ValueComponent::Variable {
                    resolved_value: matched_variable.as_ref().map(|v| v.value.clone()).clone(),
                    identifier,
                    env: if matched_variable.is_some() {
                        Some(active_env.as_ref().unwrap().name.clone())
                    } else {
                        None
                    },
                },
                value: matched_value,
            }]
        })
        .collect::<Vec<_>>()
}

fn determine_computed_matches(value: &str) -> Vec<ValuePart> {
    let computed_regex = Regex::new(r"(\{\{gen\(([[:word:]])+\)\}\})").unwrap();
    computed_regex
        .find_iter(value)
        .flat_map(|p| {
            let matched_value = p.as_str().to_string();
            let identifier = extract_computed_identifier(&matched_value);
            let computed_value = match identifier.as_str() {
                "UUID" => Some(ComputedValue::UuidV4 {
                    generated: "123-452-234".to_string(),
                }),
                "RndAlphanum" => Some(ComputedValue::RandomAlphanumeric {
                    generated: "abc123".to_string(),
                }),
                _ => None,
            };
            computed_value
                .map(|c| {
                    vec![ValuePart {
                        start: p.start() as u32,
                        end: p.end() as u32,
                        component: ValueComponent::Computed(c),
                        value: matched_value,
                    }]
                })
                .unwrap_or(Vec::new())
        })
        .collect::<Vec<_>>()
}

fn extract_computed_identifier(matched_value: &String) -> String {
    matched_value
        .chars()
        .skip(6)
        .take(matched_value.len() - 9)
        .collect()
}

fn extract_variable_identifier(matched_value: &String) -> String {
    matched_value
        .chars()
        .skip(2)
        .take(matched_value.len() - 4)
        .collect()
}

mod tests {
    use super::parse_value;
    use crate::model::{
        variables::ValueComponent,
        workspaces::{Environment, Variable},
    };

    #[test]
    pub fn should_parse_standard_vars() {
        // given

        // when
        let parts = parse_value(
            "https://abc.de/{{PATH}}?query={{MY_VAR}}&last&{{gen(UUID)}}",
            &None,
        )
        .unwrap();

        // then
        assert_eq!(
            vec![
                "https://abc.de/".to_string(),
                "{{PATH}}".to_string(),
                "?query=".to_string(),
                "{{MY_VAR}}".to_string(),
                "&last&".to_string(),
                "{{gen(UUID)}}".to_string(),
            ],
            parts
                .parts
                .iter()
                .map(|p| p.value.to_string())
                .collect::<Vec<_>>()
        );
        assert_eq!(
            vec![
                "literal".to_string(),
                "variable".to_string(),
                "literal".to_string(),
                "variable".to_string(),
                "literal".to_string(),
                "computed".to_string(),
            ],
            parts
                .parts
                .iter()
                .map(|p| match p.component {
                    ValueComponent::Literal => "literal".to_string(),
                    ValueComponent::Variable { .. } => "variable".to_string(),
                    ValueComponent::Computed(_) => "computed".to_string(),
                })
                .collect::<Vec<_>>()
        );
        assert_eq!(
            vec!["PATH".to_string(), "MY_VAR".to_string(),],
            parts
                .parts
                .iter()
                .filter_map(|p| match &p.component {
                    ValueComponent::Variable { identifier, .. } => Some(identifier.to_string()),
                    _ => None,
                })
                .collect::<Vec<_>>()
        );
    }

    #[test]
    pub fn should_parse_standard_vars_with_var_at_beginning() {
        // given

        // when
        let parts = parse_value("{{PROTOCOL}}hostname", &None).unwrap();

        // then
        assert_eq!(
            vec!["{{PROTOCOL}}".to_string(), "hostname".to_string(),],
            parts
                .parts
                .iter()
                .map(|p| p.value.to_string())
                .collect::<Vec<_>>()
        );
        assert_eq!(
            vec!["variable".to_string(), "literal".to_string(),],
            parts
                .parts
                .iter()
                .map(|p| match p.component {
                    ValueComponent::Literal => "literal".to_string(),
                    ValueComponent::Variable { .. } => "variable".to_string(),
                    ValueComponent::Computed(_) => "computed".to_string(),
                })
                .collect::<Vec<_>>()
        );
        assert_eq!(
            vec!["PROTOCOL".to_string()],
            parts
                .parts
                .iter()
                .filter_map(|p| match &p.component {
                    ValueComponent::Variable { identifier, .. } => Some(identifier.to_string()),
                    _ => None,
                })
                .collect::<Vec<_>>()
        );
    }

    #[test]
    pub fn should_parse_variable_without_active_env() {
        // given

        // when
        let parts = parse_value("{{ABC}}", &None).unwrap();

        // then
        assert_eq!(1, parts.parts.len());
        let part = parts.parts.get(0).unwrap();
        if let ValueComponent::Variable {
            identifier,
            resolved_value,
            env,
        } = &part.component
        {
            assert!(resolved_value.is_none());
            assert!(env.is_none());
            assert_eq!("ABC", identifier);
        } else {
            panic!("unexpected type");
        }
    }

    #[test]
    pub fn should_parse_variable_with_active_env_but_no_matching_var() {
        // given
        let env = Environment {
            description: None,
            name: "abc".to_string(),
            variables: vec![Variable {
                key: "key".to_string(),
                value: "value".to_string(),
                is_secret: false,
            }],
        };

        // when
        let parts = parse_value("{{ABC}}", &Some(env)).unwrap();

        // then
        assert_eq!(1, parts.parts.len());
        let part = parts.parts.get(0).unwrap();
        if let ValueComponent::Variable {
            identifier,
            resolved_value,
            env,
        } = &part.component
        {
            assert!(resolved_value.is_none());
            assert!(env.is_none());
            assert_eq!("ABC", identifier);
        } else {
            panic!("unexpected type");
        }
    }

    #[test]
    pub fn should_parse_variable_with_active_env_and_matching_var() {
        // given
        let env = Environment {
            description: None,
            name: "abc".to_string(),
            variables: vec![Variable {
                key: "ABC".to_string(),
                value: "value".to_string(),
                is_secret: false,
            }],
        };

        // when
        let parts = parse_value("{{ABC}}", &Some(env)).unwrap();

        // then
        assert_eq!(1, parts.parts.len());
        let part = parts.parts.get(0).unwrap();
        if let ValueComponent::Variable {
            identifier,
            resolved_value,
            env,
        } = &part.component
        {
            assert_eq!("value", resolved_value.as_ref().unwrap());
            assert_eq!("abc", env.as_ref().unwrap());
            assert_eq!("ABC", identifier);
        } else {
            panic!("unexpected type");
        }
    }
}
