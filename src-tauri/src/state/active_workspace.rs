use crate::model::workspaces::{Environment, Workspace};
use log::debug;
use std::sync::Mutex;

#[derive(Default)]
pub struct ActiveWorkspace {
    workspace: Mutex<Option<Workspace>>,
}

impl ActiveWorkspace {
    pub fn workspace_unchecked(&self) -> Workspace {
        self.get_locked_workspace().expect("workspace not set yet")
    }

    pub fn update(&self, new_workspace: &Workspace) {
        *self.workspace.lock().expect("requiring lock failed") = Some(new_workspace.clone());
        debug!("Updated workspace in memory");
    }

    pub fn env_by_name(&self, name: &str) -> Option<Environment> {
        self.workspace_unchecked()
            .environments
            .iter()
            .find(|e| e.name.eq(name))
            .cloned()
    }

    fn get_locked_workspace(&self) -> Option<Workspace> {
        self.workspace
            .lock()
            .expect("requiring lock failed")
            .as_ref()
            .cloned()
    }
}
